﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForRef
{

    [StructLayout(LayoutKind.Sequential)]
    struct CalibrationSet
    {
        public String KinectIrImage;
        public String CameraImage;
        public String DepthData;
        public CalibrationSet(String kinectIrImage, String cameraImage, String depthData)
        {
            KinectIrImage = kinectIrImage;
            CameraImage = cameraImage;
            DepthData = depthData;
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("Calibration/Calibration.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern bool calibrate(CalibrationSet[] calibrationSet, int size, String outputFile);


        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CalibrationSet[] sample = new CalibrationSet[]
            {
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0059_infra.jpg", "Resources/Dataset 3.2/DSC_0059.jpg", "Resources/Dataset 3.2/DSC_0059_depth.csv"),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0060_infra.jpg", "Resources/Dataset 3.2/DSC_0060.jpg", "Resources/Dataset 3.2/DSC_0060_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0061_infra.jpg", "Resources/Dataset 3.2/DSC_0061.jpg", "Resources/Dataset 3.2/DSC_0061_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0062_infra.jpg", "Resources/Dataset 3.2/DSC_0062.jpg", "Resources/Dataset 3.2/DSC_0062_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0063_infra.jpg", "Resources/Dataset 3.2/DSC_0063.jpg", "Resources/Dataset 3.2/DSC_0063_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0064_infra.jpg", "Resources/Dataset 3.2/DSC_0064.jpg", "Resources/Dataset 3.2/DSC_0064_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0065_infra.jpg", "Resources/Dataset 3.2/DSC_0065.jpg", "Resources/Dataset 3.2/DSC_0065_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0066_infra.jpg", "Resources/Dataset 3.2/DSC_0066.jpg", "Resources/Dataset 3.2/DSC_0066_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0067_infra.jpg", "Resources/Dataset 3.2/DSC_0067.jpg", "Resources/Dataset 3.2/DSC_0067_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0068_infra.jpg", "Resources/Dataset 3.2/DSC_0068.jpg", "Resources/Dataset 3.2/DSC_0068_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0069_infra.jpg", "Resources/Dataset 3.2/DSC_0069.jpg", "Resources/Dataset 3.2/DSC_0069_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0070_infra.jpg", "Resources/Dataset 3.2/DSC_0070.jpg", "Resources/Dataset 3.2/DSC_0070_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0071_infra.jpg", "Resources/Dataset 3.2/DSC_0071.jpg", "Resources/Dataset 3.2/DSC_0071_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0072_infra.jpg", "Resources/Dataset 3.2/DSC_0072.jpg", "Resources/Dataset 3.2/DSC_0072_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0073_infra.jpg", "Resources/Dataset 3.2/DSC_0073.jpg", "Resources/Dataset 3.2/DSC_0073_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0074_infra.jpg", "Resources/Dataset 3.2/DSC_0074.jpg", "Resources/Dataset 3.2/DSC_0074_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0075_infra.jpg", "Resources/Dataset 3.2/DSC_0075.jpg", "Resources/Dataset 3.2/DSC_0075_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0076_infra.jpg", "Resources/Dataset 3.2/DSC_0076.jpg", "Resources/Dataset 3.2/DSC_0076_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0077_infra.jpg", "Resources/Dataset 3.2/DSC_0077.jpg", "Resources/Dataset 3.2/DSC_0077_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0078_infra.jpg", "Resources/Dataset 3.2/DSC_0078.jpg", "Resources/Dataset 3.2/DSC_0078_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0079_infra.jpg", "Resources/Dataset 3.2/DSC_0079.jpg", "Resources/Dataset 3.2/DSC_0079_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0080_infra.jpg", "Resources/Dataset 3.2/DSC_0080.jpg", "Resources/Dataset 3.2/DSC_0080_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0081_infra.jpg", "Resources/Dataset 3.2/DSC_0081.jpg", "Resources/Dataset 3.2/DSC_0081_depth.csv" ),
		        new CalibrationSet("Resources/Dataset 3.2/DSC_0082_infra.jpg", "Resources/Dataset 3.2/DSC_0082.jpg", "Resources/Dataset 3.2/DSC_0082_depth.csv" )
	        };
            calibrate(sample, sample.Length, "calibration.yaml");
        }
    }
}
