﻿using CameraControl.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumpBox
{
    public class ServiceProvider
    {
        public ServiceProvider()
        {
            _cameraManager = new CameraDeviceManager();
            _cameraDeviceLogger = new CameraDeviceLogger();
            _storageHelper = new StorageHelper();
        }




        private List<ILog> _logProviders = new List<ILog>();

        public static void AddLogProvider(ILog logProvider)
        {
            Instance._logProviders.Add(logProvider);
        }

        private static ServiceProvider _instance;
        private CameraDeviceManager _cameraManager;
        private CameraDeviceLogger _cameraDeviceLogger;
        private StorageHelper _storageHelper;

        public static ServiceProvider Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ServiceProvider();
                return _instance;
            }
            set { _instance = value; }
        }

        public static void Log(string message)
        {
            foreach (var logger in Instance._logProviders)
            {
                logger.Log(message);
            }
        }





        public static CameraDeviceManager CameraDeviceManager { get { return Instance._cameraManager; } }

        public static CameraDeviceLogger CameraDeviceLogger { get { return Instance._cameraDeviceLogger; } }
        public static StorageHelper StorageHelper { get { return Instance._storageHelper; } }
    }
}
