﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JumpBox
{
    public delegate void JumperChangedEventHander(Object sender, JumperChangedEventArgs e);

    public class JumperChangedEventArgs : EventArgs
    {
        public JumperChangedEventArgs(int count)
        {
            this.JumperCount = count;
        }
        public int JumperCount { get; set; }
    }

}
