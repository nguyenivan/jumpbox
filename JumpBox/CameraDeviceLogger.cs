﻿using JumpBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CameraControl.Devices
{
    public class CameraDeviceLogger
    {
        public CameraDeviceLogger()
        {
            Log.LogDebug += DoLog;
            Log.LogError += DoLog;
            Log.LogInfo += DoLog;

        }

        void DoLog(Classes.LogEventArgs e)
        {
            ServiceProvider.Log(e.Message.ToString());
        }
    }
}
