﻿using CameraControl.Devices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace JumpBox
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            bool condition = false;
            if (condition)
            {
                Calibration calibration = new Calibration();
                calibration.Show();
                ServiceProvider.AddLogProvider(calibration);
            }
            else
            {
                Capture capture = new Capture();
                capture.Show();
                ServiceProvider.AddLogProvider(capture);
            }

            KinectHelper.Initialize();
        }
    }
}
