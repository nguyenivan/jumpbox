﻿using CameraControl.Devices;
using CameraControl.Devices.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace JumpBox
{
    /// <summary>
    /// Interaction logic for Calibration.xaml
    /// </summary>
    public partial class Capture : Window, ILog, INotifyPropertyChanged
    {

        [DllImport("Calibration.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern bool filter(	
            string calibrationConfig,
	        string backgroundImage,
	        string cameraImage,
	        string depthData,
	        string outputImage,
	        string bodyIndex,
	        int nearClipPlane, // in mm
	        int farClipPlane // in mm
        );

        ICameraDevice _selectedCam = null;

        public Capture()
        {

            InitializeComponent();
            TextBoxUtilities.SetAlwaysScrollToEnd(LogBox, true);
            DataContext = this;
            HeightSensitivity = 5;
            CrowdSensitivity = 5;

            ServiceProvider.CameraDeviceManager.CameraConnected += CameraDeviceManager_CameraConnected;
            ServiceProvider.CameraDeviceManager.CameraDisconnected += CameraDeviceManager_CameraDisconnected;
            KinectHelper.KinectConnected += KinectHelper_KinectConnected;
            KinectHelper.KinectDisconnected += KinectHelper_KinectDisconnected;
            ServiceProvider.StorageHelper.OnNewCapture += StorageHelper_OnNewCalibration;

            KinectHelper.JumpHelper.ClimaxReached += JumpHelper_ClimaxReached;
            KinectHelper.JumpHelper.JumperChanged += JumpHelper_JumperChanged;

            ServiceProvider.StorageHelper.InitializeCalibrationList();
            CameraLed.TurnedOn = ServiceProvider.CameraDeviceManager.ConnectToCamera();

            _leds = new List<Led>(){
                User1,
                User2,
                User3,
                User4,
                User5,
                User6
            };

            this.AutoCapture = true;

            //KinectLed.TurnedOn = KinectHelper.IsConnected;
        }

        void JumpHelper_JumperChanged(object sender, JumperChangedEventArgs e)
        {
            int jumperCount = e.JumperCount;
            for (int i = 0; i < _leds.Count;i++ )
            {
                if (i < jumperCount)
                {
                    _leds[i].TurnedOn = true;
                }
                else
                {
                    _leds[i].TurnedOn = false;
                }
            }

        }

        void JumpHelper_ClimaxReached(object sender, EventArgs e)
        {
            if (this.AutoCapture)
            {
                DoCapture();
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public ImageSource KinectImage
        {
            get
            {
                return KinectHelper.ImageSource;
            }
        }

        void StorageHelper_OnNewCalibration(object sender, TupleEventArgs e)
        {
            AddNewCalibrationTuple(e.FileTuple);
        }

        private void KinectHelper_KinectDisconnected(object sender, EventArgs e)
        {
            this.KinectLed.TurnedOn = false;
        }

        private void KinectHelper_KinectConnected(object sender, EventArgs e)
        {
            this.KinectLed.TurnedOn = true;
        }

        void CameraDeviceManager_CameraDisconnected(CameraControl.Devices.ICameraDevice cameraDevice)
        {
            if (_selectedCam == cameraDevice) _selectedCam = null;
            this.Dispatcher.BeginInvoke(new Action(() => this.CameraLed.TurnedOn = false));
        }

        void CameraDeviceManager_CameraConnected(CameraControl.Devices.ICameraDevice cameraDevice)
        {
            _selectedCam = cameraDevice;
            if (_selectedCam != null)
            {
                _selectedCam.PhotoCaptured += SelectedCam_PhotoCaptured;
                _selectedCam.CaptureCompleted += SelectedCam_CaptureCompleted;
            }
            this.Dispatcher.BeginInvoke(new Action(() => this.CameraLed.TurnedOn = true));
        }

        private void SelectedCam_CaptureCompleted(object sender, EventArgs e)
        {
        }

        private void SelectedCam_PhotoCaptured(object sender, CameraControl.Devices.Classes.PhotoCapturedEventArgs eventArgs)
        {
            try
            {
                _capturedEventArgs = eventArgs;
                Tuple<string, string, string, string> fileTuple = ServiceProvider.StorageHelper.GetNextFileTuple<Tuple<string, string, string, string>>(
                    new Tuple<string, string, string, string>("camera", "infra", "depth", "bindex"),
                    new Tuple<string, string, string, string>("jpg", "jpg", "csv", "csv"),
                    3, ServiceProvider.StorageHelper.CaptureFolder
                 );
                _selectedCam.IsBusy = true;
                _selectedCam.TransferFile(_capturedEventArgs.Handle, fileTuple.Item1);
                KinectHelper.SaveIrFrame(fileTuple.Item2);
                KinectHelper.SaveDepthFrame(fileTuple.Item3);
                KinectHelper.SaveBodyIndexFrame(fileTuple.Item4);
                string outputFile = ServiceProvider.StorageHelper.GetOutputFromCameraFile(fileTuple.Item1);
                bool success = filter(
                    ServiceProvider.StorageHelper.ConfigurationFilePath,
                    ServiceProvider.StorageHelper.BackgroundFilePath,
                    fileTuple.Item1,
                    fileTuple.Item3,
                    outputFile,
                    fileTuple.Item4,
                    2000,
                    3000
                    );

            }
            finally
            {
                _selectedCam.IsBusy = false;
            }
        }

        public bool AutoCapture
        {
            get { return _autoCapture; }
            set
            {
                _autoCapture = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AutoCapture"));
            }
        }


        public int HeightSensitivity
        {
            get { return KinectHelper.JumpHelper.HeightSensitivity; }
            set
            {
                //_heightSensitivity = value;
                KinectHelper.JumpHelper.HeightSensitivity = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CaptureRepeat"));
            }
        }

        public int CrowdSensitivity
        {
            get { return KinectHelper.JumpHelper.CrowdSensitivity; }
            set
            {
                KinectHelper.JumpHelper.CrowdSensitivity = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CaptureInterval"));
            }
        }

        public void Log(string message)
        {
            this.Dispatcher.BeginInvoke(new Action(() => LogBox.AppendText(message + Environment.NewLine)));
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private bool _autoCapture;
        //private int _heightSensitivity;
        //private int _crowdSensitivity;
        private PhotoCapturedEventArgs _capturedEventArgs;
        private DispatcherTimer _timer = new DispatcherTimer();
        private List<Led> _leds;


        private void ButtonSpinner_Spin(object sender, Xceed.Wpf.Toolkit.SpinEventArgs e)
        {
            ButtonSpinner spinner = (ButtonSpinner)sender;
            TextBox txtBox = (TextBox)spinner.Content;

            int value = String.IsNullOrEmpty(txtBox.Text) ? 0 : Convert.ToInt32(txtBox.Text);
            if (e.Direction == SpinDirection.Increase)
                value++;
            else
                value--;
            txtBox.Text = value.ToString();
        }

        private void AddNewCalibrationTuple(Tuple<string, string> fileTuple)
        {
            var newRow = new RowDefinition();
            newRow.Height = new GridLength(64);
            ImageSourceConverter convertImUri = new ImageSourceConverter();
            var kIm = fileTuple.Item1;
            var cIm = fileTuple.Item2;
            var kImSrc = (ImageSource)convertImUri.ConvertFromString(kIm);
            var cImSrc = (ImageSource)convertImUri.ConvertFromString(cIm);

            var kImCtrl = new Image();
            kImCtrl.Source = kImSrc;

            var cImCtrl = new Image();
            cImCtrl.Source = cImSrc;



            var currentRow = this.Preview.RowDefinitions.Count;

            Grid.SetColumn(kImCtrl, 0);
            Grid.SetRow(kImCtrl, currentRow);
            Grid.SetColumn(cImCtrl, 1);
            Grid.SetRow(cImCtrl, currentRow);

            this.Preview.RowDefinitions.Add(newRow);
            this.Preview.Children.Add(kImCtrl);
            this.Preview.Children.Add(cImCtrl);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!this.AutoCapture)
            {
                DoCapture();
            }
        }

        DateTime _lastCapture = DateTime.Now;
        TimeSpan _minCaptureGap = new TimeSpan(0, 0, 3);
        private void DoCapture()
        {
            if (DateTime.Now - _lastCapture < _minCaptureGap)
            {
                ServiceProvider.Log(string.Format("Please wait at least {0} seconds after each shot.", _minCaptureGap.Seconds));
                return;
            }
            _lastCapture = DateTime.Now;
            if (!KinectHelper.IsConnected || _selectedCam == null)
            {
                ServiceProvider.Log("Please connect both Kinect and Camera to begin capture");
                return;
            }
            KinectHelper.Capture();
            _selectedCam.CapturePhoto();
        }

        private void Calib_Click(object sender, RoutedEventArgs e)
        {
            Calibration calibration = new Calibration();
            calibration.Show();
            ServiceProvider.AddLogProvider(calibration);
            this.Close();
        }
    }
}
