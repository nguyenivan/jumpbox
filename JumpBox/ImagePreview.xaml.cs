﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JumpBox
{
    /// <summary>
    /// Interaction logic for ImagePreview.xaml
    /// </summary>
    public partial class ImagePreview : Window
    {
        Window _parent = null;
        public ImagePreview(Window parent, ImageSource imageSource)
        {
            InitializeComponent();
            this.ImageControl.Source = imageSource;
            _parent = parent;
        }

        private void ImageControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_parent != null)
            {
                _parent.Focus();
            }
        }
        
    }
}
