﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumpBox
{
    public class StorageHelper
    {
        private string _calibration;
        FileSystemWatcher _calibWatcher;
        private const string DEFAULT_CALIBRATION_FOLDER = "Calibration";
        private const string DEFAULT_CAPTURE_FOLDER = "Capture";
        private const string DEFAULT_BACKGROUND_FILE_PATH = "background.jpg";
        private const string DEFAULT_CONFIGURATION_FILE_PATH = "calibration.yaml";

        private const int SEQ_LENGTH = 3;

        public delegate void CalibrationTupleHandler(object sender, TupleEventArgs e);
        public event CalibrationTupleHandler OnNewCalibration = delegate { };
        public event CalibrationTupleHandler OnNewCapture = delegate { };
        private FileSystemWatcher _captureWatcher;

        public StorageHelper()
        {
            CalibrationFolder = DEFAULT_CALIBRATION_FOLDER;
            CaptureFolder = DEFAULT_CAPTURE_FOLDER;
            BackgroundFilePath = DEFAULT_BACKGROUND_FILE_PATH;
            ConfigurationFilePath = DEFAULT_CONFIGURATION_FILE_PATH;

            _calibWatcher = new FileSystemWatcher();
            _calibWatcher.Path = CalibrationFolder;

            _captureWatcher = new FileSystemWatcher();
            _captureWatcher.Path = CaptureFolder;
            /* Watch for changes in LastAccess and LastWrite times, and
               the renaming of files or directories. */
            _calibWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _captureWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            // Only watch text files.
            _calibWatcher.Filter = "*.jpg";
            _calibWatcher.Created += calibWatcher_Created;
            _captureWatcher.Filter = "*.jpg";
            _captureWatcher.Created += captureWatcher_Created;
        }

        private void captureWatcher_Created(object sender, FileSystemEventArgs e)
        {
            string im = e.FullPath;
            var t = GetFileTuple(im);
            if (t != null)
            {
                OnNewCapture(this, new TupleEventArgs(t));
            }
        }

        void calibWatcher_Created(object sender, FileSystemEventArgs e)
        {
            string im = e.FullPath;
            var t = GetFileTuple(im);
            if (t != null)
            {
                OnNewCalibration(this, new TupleEventArgs(t));
            }
        }


        public string CalibrationFolder
        {
            get { return _calibration; }
            set {
                if (!Directory.Exists(value))
                {
                    Directory.CreateDirectory(value);
                }
                _calibration = value; 
            }
        }

        public void InitializeCalibrationList()
        {
            var fileList = GetCalibrationFiles();
            foreach (var t in fileList)
            {
                OnNewCalibration(this, new TupleEventArgs(t));
            }
        }

        public List<Tuple<string,string>>  GetCalibrationFiles() {
            var ret = new List<Tuple<string,string>>();

            foreach (var kIm in Directory.EnumerateFiles(_calibration, "infra???.jpg")){
                //Analize the name
                var t = GetFileTuple(kIm);
                if (t != null)
                {
                    ret.Add(t);
                }
            }
            return ret;
        }

        private Tuple<string, string> GetFileTuple(string filePath)
        {
            string kSuff = "infra";
            string cSuff = "camera";
            string seq, kIm, cIm;
            string fileName = Path.GetFileNameWithoutExtension(filePath);
            string fileFolder = Path.GetDirectoryName(filePath);
            if (fileName.StartsWith(kSuff))
            {
                kIm = filePath;
                seq = fileName.Substring(kSuff.Length);
                cIm = Path.Combine(fileFolder, string.Format("{0}{1}.jpg", cSuff, seq));

            } else if (fileName.StartsWith(cSuff)){
                cIm = filePath;
                seq = fileName.Substring(cIm.Length);
                kIm = Path.Combine(fileFolder, string.Format("{0}{1}.jpg", kSuff, seq));
            }
            else { return null; }

            if (File.Exists(kIm) && File.Exists(cIm))
            {
                return new Tuple<string, string>(kIm, cIm);
            }
            else
            {
                return null;
            }
        }


        public T GetNextFileTuple<T>(T prefix, T suffix, int seqDigit = 3, string folder="")
        {
            if (string.IsNullOrEmpty(folder))
            {
                folder = this.CalibrationFolder;
            }
            string[] prefixArray;
            string[] suffixArray;
            if (typeof(T) == typeof(Tuple<string, string>))
            {
                var t = prefix as Tuple<string, string>;
                prefixArray = new string[] { t.Item1, t.Item2 };
                var s = suffix as Tuple<string, string>;
                suffixArray = new string[] { s.Item1, s.Item2};
            }
            else if (typeof(T) == typeof(Tuple<string, string, string>))
            {
                var t = prefix as Tuple<string, string, string>;
                prefixArray = new string[] { t.Item1, t.Item2, t.Item3 };
                var s = suffix as Tuple<string, string, string>;
                suffixArray = new string[] { s.Item1, s.Item2, s.Item3 };
            }
            else if (typeof(T) == typeof(Tuple<string, string, string, string>))
            {
                var t = prefix as Tuple<string, string, string, string>;
                prefixArray = new string[] { t.Item1, t.Item2, t.Item3, t.Item4 };
                var s = suffix as Tuple<string, string, string, string>;
                suffixArray = new string[] { s.Item1, s.Item2, s.Item3, s.Item4 };
            } else
            {
                throw new Exception("This method only supports a Tuple with 2-4 items in it");
            }


            string seqFormat = string.Format("D{0}", seqDigit);

            int nextStringSeq = 0;
            while (nextStringSeq++ < int.MaxValue / 100)
            {
                int existCount = 0;
                for (int i = 0; i < prefixArray.Length; i++ )
                {
                    var f = Path.Combine(_calibration, string.Format("{0}{1}.{2}", prefixArray[i], nextStringSeq.ToString(seqFormat), suffixArray[i]));
                    if (File.Exists(f))
                    {
                        existCount++;
                    }
                }
                if (existCount == 0)
                {
                    break;
                }
            }

            if (typeof(T) == typeof(Tuple<string, string>))
            {
                return (T)Convert.ChangeType(new Tuple<string, string>(
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[0], nextStringSeq.ToString(seqFormat),suffixArray[0])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[1], nextStringSeq.ToString(seqFormat),suffixArray[1]))
                ), typeof(T));
            }
            else if (typeof(T) == typeof(Tuple<string, string, string>))
            {
                return (T)Convert.ChangeType(new Tuple<string, string, string>(
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[0], nextStringSeq.ToString(seqFormat), suffixArray[0])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[1], nextStringSeq.ToString(seqFormat), suffixArray[1])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[2], nextStringSeq.ToString(seqFormat), suffixArray[2]))
                ), typeof(T));                                                                                          
            }                                                                                                           
            else                                                                                                        
            {                                                                                                           
                return (T)Convert.ChangeType(new Tuple<string, string, string, string>(                                 
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[0], nextStringSeq.ToString(seqFormat), suffixArray[0])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[1], nextStringSeq.ToString(seqFormat), suffixArray[1])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[2], nextStringSeq.ToString(seqFormat), suffixArray[2])),
                    Path.Combine(folder, string.Format("{0}{1}.{2}", prefixArray[3], nextStringSeq.ToString(seqFormat), suffixArray[3]))
                ), typeof(T));
            } 
        }

        public string BackgroundFilePath { get; set; }

        public string CaptureFolder { get; set; }

        public string GetOutputFromCameraFile(string cameraFile)
        {
            string fileName = Path.GetFileNameWithoutExtension(cameraFile);
            int seq;
            if (int.TryParse(fileName.Substring(fileName.Length - SEQ_LENGTH), out seq))
            {
                string fileFolder = Path.GetDirectoryName(cameraFile);
                string seqFormat = string.Format("D{0}", SEQ_LENGTH);
                string ret = Path.Combine(fileFolder, string.Format("output{0}.jpg", seq.ToString(seqFormat)));
                return ret;
            }
            else
            {
                throw new Exception("Camera image file should be in the format camera123.jpg");
            }
        }

        public string ConfigurationFilePath { get; set; }
    }
}
