﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JumpBox
{
    /// <summary>
    /// Interaction logic for Led.xaml
    /// </summary>
    public partial class Led : UserControl
    {
        public Led()
        {
            this.InitializeComponent();
        }

        public static readonly DependencyProperty TurnedOnProperty =
            DependencyProperty.Register("TurnedOn",
                                         typeof(bool),
                                         typeof(Led),
                                         null);
        public bool TurnedOn
        {
            get { return (bool)GetValue(TurnedOnProperty); }
            set { SetValue(TurnedOnProperty, value); ChangeState(value); }
        }

        private void ChangeState(bool turnedOn)
        {
            if (turnedOn)
            {
                this.BigCircle.Style = (Style)this.FindResource("GreenBrush");
            }
            else
            {
                this.BigCircle.Style = (Style)this.FindResource("RedBrush");
            }
        }

    }
}