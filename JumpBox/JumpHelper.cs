﻿using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace JumpBox
{

    public class JumpHelper : IDisposable
    {
        public event JumperChangedEventHander JumperChanged = delegate { };
        public event EventHandler ClimaxReached = delegate { };

        KinectSensor _kinect;

        /// <summary> Path to the gesture database that was trained with VGB </summary>
        private readonly string _gestureDatabase = @"Gestures\JumpBox.gbd";

        /// <summary> Name of the discrete gesture in the database that we want to track </summary>
        private readonly string _jumpGestureName = "JumpD";

        private BodyFrameReader _bodyReader;
        private Body[] _bodies;
        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        private Dictionary<ulong, VisualGestureBuilderFrameSource> _sourceDict = null;
        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        private Dictionary<ulong, VisualGestureBuilderFrameReader> _readerDict = null;

        private Dictionary<ulong, bool> _climaxFlags = null;
        private Dictionary<ulong, bool> _jumpFlags = null;

        private int _crowdSensitivity;
        private int _heightSensitivity;

        private DispatcherTimer _timer = new DispatcherTimer();
        private bool _lastSeenEmpty = true;


        public JumpHelper(KinectSensor kinect)
        {
            if (kinect == null)
            {
                throw new ArgumentNullException("kinect", "Kinect sensor is null.");
            }
            _kinect = kinect;
            _bodyReader = _kinect.BodyFrameSource.OpenReader();
            _bodyReader.FrameArrived += _bodyReader_FrameArrived;
            CrowdSensitivity = 5; // 50% of jumpers reach height
            HeightSensitivity = 5; // 0.5 confidence
            JumpThreshold = 1;
        }

        public int JumpThreshold { get; set; }

        // From 1 to 10
        public int HeightSensitivity
        {
            get { return _heightSensitivity; }
            set { _heightSensitivity = value; }
        }

        // From 1 to 10
        public int CrowdSensitivity
        {
            get { return _crowdSensitivity; }
            set
            {
                _crowdSensitivity = value;
                //_climaxQueue = new FixedSizedQueue<DateTime>(value);
            }
        }

        private void InitializeReaders(IEnumerable<Body> bodyEnum)
        {
            if (_sourceDict != null || _readerDict != null)
            {
                throw new Exception("Cannot initialize frame source or frame twice.");
            }

            //_climaxFlags = Enumerable.Repeat<bool>(false, bodyEnum.Count()).ToArray();
            //_jumpFlags = Enumerable.Repeat<bool>(false, bodyEnum.Count()).ToArray();

            _sourceDict = new Dictionary<ulong, VisualGestureBuilderFrameSource>();
            _readerDict = new Dictionary<ulong, VisualGestureBuilderFrameReader>();
            _climaxFlags = new Dictionary<ulong, bool>();
            _jumpFlags = new Dictionary<ulong, bool>();
            // load the 'JumpD' gesture from the gesture database
            ulong maxId = ulong.MaxValue;
            using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(_gestureDatabase))
            {
                foreach (var body in bodyEnum)
                {
                    ulong trackingId = body.IsTracked ? body.TrackingId : --maxId;
                    var source = new VisualGestureBuilderFrameSource(_kinect, trackingId);
                    _sourceDict.Add(trackingId, source);
                    var reader = source.OpenReader();
                    reader.IsPaused = false;
                    reader.FrameArrived += reader_FrameArrived;
                    _readerDict.Add(trackingId, reader);
                    source.AddGestures(database.AvailableGestures);

                    _climaxFlags.Add(trackingId, false);
                    _jumpFlags.Add(trackingId, false);
                }

            }
        }

        void reader_FrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;

                    if (discreteResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in frame.VisualGestureBuilderFrameSource.Gestures)
                        {
                            if (gesture.Name.Equals(_jumpGestureName) && gesture.GestureType == GestureType.Discrete)
                            {
                                DiscreteGestureResult result = null;
                                discreteResults.TryGetValue(gesture, out result);
                                if (result != null)
                                {
                                    ProcessGesture(result, frame.VisualGestureBuilderFrameSource.TrackingId);
                                }
                            }
                        }
                    }
                }
            }
        }

        TimeSpan _climaxMinGap = new TimeSpan(0, 0, 1);
        DateTime _lastClimax = DateTime.Now;

        private void ProcessGesture(DiscreteGestureResult result, ulong trackingId)
        {
            if (!_climaxFlags.Keys.Contains(trackingId))
            {
                throw new Exception("Climax dictionary is not updated in time");
            }

            if (result.Detected && result.Confidence * 10 > this.JumpThreshold) // is jumping
            {
                _jumpFlags[trackingId] = true;
                Console.WriteLine(result.Confidence * 10);
            }
            else
            {
                _jumpFlags[trackingId] = false;
            }
            if (result.Detected && result.Confidence * 10 > this.HeightSensitivity) // reached climax
            {
                _climaxFlags[trackingId] = true;
            }
            else
            {
                _climaxFlags[trackingId] = false;
            }

            if (10 * _climaxFlags.Where(x => x.Value).Count() / _climaxFlags.Count + 1 > this.CrowdSensitivity 
                && _jumpFlags.Any(x => x.Value 
                && DateTime.Now - _lastClimax > _climaxMinGap)
                )
            {
                _lastClimax = DateTime.Now;
                ClimaxReached(this, new EventArgs());
            }

        }

        void _bodyReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (_bodies == null)
                    {
                        _bodies = new Body[bodyFrame.BodyCount];
                        bodyFrame.GetAndRefreshBodyData(_bodies);
                        InitializeReaders(_bodies);
                    }
                    else
                    {
                        bodyFrame.GetAndRefreshBodyData(_bodies);
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                List<ulong> receivedTrackingIdList = _bodies.Where(x => x.IsTracked).Select(x => x.TrackingId).ToList();
                List<ulong> newTrackingIdList = _bodies.Where(x => x.IsTracked && !_sourceDict.Keys.Contains(x.TrackingId)).Select(x => x.TrackingId).ToList();
                List<ulong> lostTrackingIdList = _sourceDict.Keys.Where(x => !receivedTrackingIdList.Contains(x)).ToList();

                int newIdCount = 0;
                //_bodies.Select(x => x.TrackingId).Where(x => _sourceDict.Keys.Contains(x)).ToList();
                foreach (Body body in _bodies)
                {
                    if (body.IsTracked)
                    {
                        if (newTrackingIdList.Contains(body.TrackingId)) // swap this to dictionaries
                        {
                            ulong selectedId = lostTrackingIdList[newIdCount++];
                            VisualGestureBuilderFrameSource selectedSource = _sourceDict[selectedId];
                            _sourceDict.Remove(selectedId);
                            selectedSource.TrackingId = body.TrackingId;
                            _sourceDict.Add(body.TrackingId, selectedSource);
                            VisualGestureBuilderFrameReader selectedReader = _readerDict[selectedId];
                            _readerDict.Remove(selectedId);
                            _readerDict.Add(body.TrackingId, selectedReader);
                        }
                    }
                }

                foreach (ulong trackingId in newTrackingIdList)
                {
                    _climaxFlags.Add(trackingId, false);
                    _jumpFlags.Add(trackingId, false);
                }

                foreach (ulong trackingId in lostTrackingIdList)
                {
                    if (_climaxFlags.ContainsKey(trackingId))
                    {
                        _climaxFlags.Remove(trackingId);
                    }
                    if (_jumpFlags.ContainsKey(trackingId))
                    {
                        _jumpFlags.Remove(trackingId);
                    }
                }

                if (!_lastSeenEmpty && lostTrackingIdList.Count == _sourceDict.Count)
                {
                    JumperChanged(this, new JumperChangedEventArgs(0));
                    _lastSeenEmpty = true;
                }
                else if (newTrackingIdList.Count > 0)
                {
                    JumperChanged(this, new JumperChangedEventArgs(_sourceDict.Keys.Where(
                        x => receivedTrackingIdList.Contains(x)).Count()));
                    _lastSeenEmpty = false;
                }

            }
        }

        public void Dispose()
        {
            if (_bodyReader != null)
            {
                _bodyReader.Dispose();
                _bodyReader = null;
            }

            if (_readerDict != null)
            {
                foreach (KeyValuePair<ulong, VisualGestureBuilderFrameReader> pair in _readerDict)
                {
                    pair.Value.Dispose();
                    _readerDict.Remove(pair.Key);
                }
                _readerDict = null;
            }

            if (_sourceDict != null)
            {
                foreach (KeyValuePair<ulong, VisualGestureBuilderFrameSource> pair in _sourceDict)
                {
                    pair.Value.Dispose();
                    _sourceDict.Remove(pair.Key);
                }
                _sourceDict = null;
            }
        }

    }
}
