﻿using Microsoft.Kinect;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace JumpBox
{

    public enum KinectFrameType
    {
        Color = 0x1,
        Infrared = 0x2,
        Depth = 0x4,
        BodyIndex = 0x8,
        Body = 0x10
    }

    public class KinectHelper : IDisposable
    {
        private KinectSensor _kinectSensor;
        private static KinectHelper _instance;
        private ColorFrameReader _colorFrameReader;
        private FrameDescription _colorFrameDescription = null;
        private IntPtr _a = IntPtr.Zero, _b = IntPtr.Zero, _c = IntPtr.Zero, _d = IntPtr.Zero, _aCopy = IntPtr.Zero, _bCopy = IntPtr.Zero, _cCopy = IntPtr.Zero;
        private bool _isListening;
        private bool _isDisposed;
        private MultiSourceFrameReader _multiFrameReader;
        private DepthFrameReader _depthFrameReader;
        private FrameDescription _depthFrameDescription;
        private InfraredFrameReader _irFrameReader;
        private FrameDescription _irFrameDescription;
        private bool _displayIr = true;
        private WriteableBitmap _realtimeImage = null;

        /// <summary>
        /// Map depth range to byte range
        /// </summary>
        private const int MapDepthToByte = 8000 / 256;
        float _irOutputMax = 1.0f, _irSourceMax = (float)ushort.MaxValue, _irSourceScale = 3.0f, _irOutMin = 0.01f;

        public static event EventHandler KinectConnected = delegate { };
        public static event EventHandler KinectDisconnected = delegate { };
        private JumpHelper _jumpHelper;
        private BodyIndexFrameReader _bindexFrameReader;
        private FrameDescription _bindexFrameDescription;
        private IntPtr _dCopy;


        public static WriteableBitmap ImageSource
        {
            get { return Instance._realtimeImage; }
        }

        public static JumpHelper JumpHelper
        {
            get { return Instance._jumpHelper; }
            set { Instance._jumpHelper = value; }
        }

        public KinectHelper()
        {
            //Open Kinect
            this.IsListening = true;
            _kinectSensor = KinectSensor.GetDefault();
            if (this._kinectSensor != null)
            {
                this._kinectSensor.Open();
                _multiFrameReader = _kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.BodyIndex);
                _multiFrameReader.MultiSourceFrameArrived += MultiSourceFrameArrived;
                _colorFrameReader = _kinectSensor.ColorFrameSource.OpenReader();
                _depthFrameReader = _kinectSensor.DepthFrameSource.OpenReader();
                _depthFrameDescription = _depthFrameReader.DepthFrameSource.FrameDescription;
                _irFrameReader = _kinectSensor.InfraredFrameSource.OpenReader();
                _irFrameDescription = _irFrameReader.InfraredFrameSource.FrameDescription;
                _bindexFrameReader = _kinectSensor.BodyIndexFrameSource.OpenReader();
                _bindexFrameDescription = _bindexFrameReader.BodyIndexFrameSource.FrameDescription;

                _kinectSensor.IsAvailableChanged += _kinectSensor_IsAvailableChanged;

                //_realtimeImage = BitmapFactory.New(_irFrameDescription.Width, _irFrameDescription.Height);
                _realtimeImage = new WriteableBitmap(_irFrameDescription.Width, _irFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgra32, null);

            }

            // Initialize JumpHelper
            _jumpHelper = new JumpHelper(_kinectSensor);

        }

        public static void Initialize()
        {
            var o = Instance;
        }

        void MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (!this.IsListening) return;

            // Get a reference to the multi-frame
            var reference = e.FrameReference.AcquireFrame();

            // Open color frame
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    if (null == _colorFrameDescription)
                    {
                        _colorFrameDescription = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                    }
                    uint bufferLength = (uint)(_colorFrameDescription.LengthInPixels * _colorFrameDescription.BytesPerPixel);
                    if (_a == IntPtr.Zero)
                    {
                        _a = Marshal.AllocHGlobal((int)bufferLength);
                    }
                    frame.CopyConvertedFrameDataToIntPtr(_a, bufferLength, ColorImageFormat.Bgra);
                }
            }

            // Open depth frame
            using (var frame = reference.DepthFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    uint bufferLength = (uint)(_depthFrameDescription.LengthInPixels * _depthFrameDescription.BytesPerPixel);
                    if (_b == IntPtr.Zero)
                    {
                        _b = Marshal.AllocHGlobal((int)bufferLength);
                    }
                    frame.CopyFrameDataToIntPtr(_b, bufferLength);
                }
            }

            // Open infrared frame
            using (var frame = reference.InfraredFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    uint bufferLength = (uint)(_irFrameDescription.LengthInPixels * _irFrameDescription.BytesPerPixel);
                    if (_c == IntPtr.Zero)
                    {
                        _c = Marshal.AllocHGlobal((int)bufferLength);
                    }
                    frame.CopyFrameDataToIntPtr(_c, bufferLength);
                    if (_displayIr)
                    {
                        DisplayIrBitmap(_c, bufferLength, _irOutputMax, _irSourceMax, _irSourceScale, _irOutMin);
                    }
                }
            }

            // Open body index frame
            using (var frame = reference.BodyIndexFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    uint bufferLength = (uint)(_bindexFrameDescription.LengthInPixels * _bindexFrameDescription.BytesPerPixel);
                    if (_d == IntPtr.Zero)
                    {
                        _d = Marshal.AllocHGlobal((int)bufferLength);
                    }
                    frame.CopyFrameDataToIntPtr(_d, bufferLength);
                }
            }
        }

        /// <summary>
        /// Set to false to stop listening to new frame event in order to save on CPU and RAM
        /// </summary>
        public bool IsListening
        {
            get
            {
                return _isListening;
            }
            set
            {
                //if (!_isListening && value)
                //{
                //    _isDirty = true;
                //}
                _isListening = value;
            }
        }

        public static KinectHelper Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new KinectHelper();
                return _instance;
            }
            set { _instance = value; }
        }

        public static bool Capture()
        {
            return _instance.DoCapture();
        }


        /// <summary>
        /// Copy fresh buffer of color frame to a place that can be save to disk
        /// </summary>
        /// <returns></returns>
        public bool DoCapture()
        {
            if (_colorFrameDescription == null) return false;
            int cBufferLength = (int)(_colorFrameDescription.LengthInPixels * _colorFrameDescription.BytesPerPixel);
            int dBufferLength = (int)(_depthFrameDescription.LengthInPixels * _depthFrameDescription.BytesPerPixel);
            int iBufferLength = (int)(_irFrameDescription.LengthInPixels * _irFrameDescription.BytesPerPixel);
            int bBufferLength = (int)(_bindexFrameDescription.LengthInPixels * _bindexFrameDescription.BytesPerPixel);

            if (_aCopy == IntPtr.Zero)
            {
                _aCopy = Marshal.AllocHGlobal(cBufferLength);
            }
            if (_bCopy == IntPtr.Zero)
            {
                _bCopy = Marshal.AllocHGlobal(dBufferLength);
            }
            if (_cCopy == IntPtr.Zero)
            {
                _cCopy = Marshal.AllocHGlobal(iBufferLength);
            }
            if (_dCopy == IntPtr.Zero)
            {
                _dCopy = Marshal.AllocHGlobal(bBufferLength);
            }

            AForge.SystemTools.CopyUnmanagedMemory(_aCopy, _a, cBufferLength);
            AForge.SystemTools.CopyUnmanagedMemory(_bCopy, _b, dBufferLength);
            AForge.SystemTools.CopyUnmanagedMemory(_cCopy, _c, iBufferLength);
            AForge.SystemTools.CopyUnmanagedMemory(_dCopy, _d, bBufferLength);
            return true;
        }


        /// <summary>
        /// Save the color frame to file
        /// </summary>
        /// <param name="Path">Full path to file</param>
        /// <returns>True if save successfully</returns>
        public bool SaveFrames(string path)
        {
            if (!_kinectSensor.IsAvailable)
            {
                ServiceProvider.Log(string.Format("ERROR: Kinect is not ready, skipping file {0}", path));
                return false;
            }
            uint dBufferLength = (uint)(_depthFrameDescription.LengthInPixels * _depthFrameDescription.BytesPerPixel);
            uint iBufferLength = (uint)(_irFrameDescription.LengthInPixels * _irFrameDescription.BytesPerPixel);
            var basePath = Path.GetDirectoryName(path);
            var baseName = Path.GetFileNameWithoutExtension(path);
            var baseExt = Path.GetExtension(path);
            try
            {
                var colorName = Path.Combine(basePath, string.Format("{0}_color{1}", baseName, baseExt));
                if (_aCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No color data, skipping file {0}", colorName));
                }
                else
                {
                    using (Bitmap bm = new Bitmap(_colorFrameDescription.Width, _colorFrameDescription.Height,
                        _colorFrameDescription.Width * (int)_colorFrameDescription.BytesPerPixel, System.Drawing.Imaging.PixelFormat.Format32bppRgb, _aCopy))
                    {
                        bm.RotateFlip(RotateFlipType.RotateNoneFlipX);
                        bm.Save(colorName);
                    }
                    ServiceProvider.Log(string.Format("LOG: Color frame saved to file {0}", colorName));
                }

                var depthName = Path.Combine(basePath, string.Format("{0}_depth{1}", baseName, ".csv"));
                if (_bCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No depth data, skipping file {0}", path));
                }
                else
                {
                    //Instance.SaveDepthBitmap(_bCopy, dBufferLength, _minDepth, _maxDepth, depthName);
                    Instance.SaveDepthData(_bCopy, dBufferLength, depthName);
                    ServiceProvider.Log(string.Format("LOG: Depth frame saved to file {0}", depthName));
                }

                var infraName = Path.Combine(basePath, string.Format("{0}_infra{1}", baseName, baseExt));
                if (_cCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No infra data, skipping file {0}", path));
                }
                else
                {
                    Instance.SaveIrBitmap(_cCopy, iBufferLength, _irOutputMax, _irSourceMax, _irSourceScale, _irOutMin, infraName);
                    ServiceProvider.Log(string.Format("LOG: Infra frame saved to file {0}", infraName));
                }
                return true;
            }
            catch (Exception ex)
            {
                ServiceProvider.Log(ex.Message);
                return false;
            }
        }

        public static bool SaveIrFrame(string path)
        {
            return _instance.DoSaveIrFrame(path);
        }

        public static bool SaveDepthFrame(string path)
        {
            return _instance.DoSaveDepthFrame(path);
        }

        public static bool SaveBodyIndexFrame(string path)
        {
            return _instance.DoSaveBodyIndexFrame(path);
        }

        private bool DoSaveBodyIndexFrame(string path)
        {
            if (!_kinectSensor.IsAvailable)
            {
                ServiceProvider.Log(string.Format("ERROR: Kinect is not ready, skipping file {0}", path));
                return false;
            }
            uint bufferLength = (uint)(_bindexFrameDescription.LengthInPixels * _bindexFrameDescription.BytesPerPixel);
            try
            {
                if (_dCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No body index data, skipping file {0}", path));
                }
                else
                {
                    SaveBodyIndexData(_dCopy, bufferLength, path);
                    ServiceProvider.Log(string.Format("LOG: body index frame saved to file {0}", path));
                }
                return true;
            }
            catch (Exception ex)
            {
                ServiceProvider.Log(ex.Message);
                return false;
            }
        }

        public void SaveBodyIndexData(IntPtr intPtr, uint bufferLength, string path)
        {
            int width = _bindexFrameDescription.Width;
            int height = _bindexFrameDescription.Height;
            var data = new byte[width * height];
            Marshal.Copy(intPtr, data, 0, width * height);
            using (var sw = new StreamWriter(path, false))
            {
                sw.Write(string.Join(",", data));
            }
        }

        public bool DoSaveDepthFrame(string path)
        {
            if (!_kinectSensor.IsAvailable)
            {
                ServiceProvider.Log(string.Format("ERROR: Kinect is not ready, skipping file {0}", path));
                return false;
            }
            uint bufferLength = (uint)(_depthFrameDescription.LengthInPixels * _depthFrameDescription.BytesPerPixel);
            try
            {
                if (_bCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No depth data, skipping file {0}", path));
                }
                else
                {
                    SaveDepthData(_bCopy, bufferLength, path);
                    ServiceProvider.Log(string.Format("LOG: Depth frame saved to file {0}", path));
                }
                return true;
            }
            catch (Exception ex)
            {
                ServiceProvider.Log(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Save the color frame to file
        /// </summary>
        /// <param name="Path">Full path to file</param>
        /// <returns>True if save successfully</returns>
        public bool DoSaveIrFrame(string path)
        {
            if (!_kinectSensor.IsAvailable)
            {
                ServiceProvider.Log(string.Format("ERROR: Kinect is not ready, skipping file {0}", path));
                return false;
            }
            uint bufferLength = (uint)(_irFrameDescription.LengthInPixels * _irFrameDescription.BytesPerPixel);
            try
            {
                if (_cCopy == IntPtr.Zero)
                {
                    ServiceProvider.Log(string.Format("ERROR: No infra data, skipping file {0}", path));
                }
                else
                {
                    SaveIrData(_cCopy, bufferLength, string.Format("{0}.csv", path));
                    SaveIrBitmap(_cCopy, bufferLength, _irOutputMax, _irSourceMax, _irSourceScale, _irOutMin, path);
                    ServiceProvider.Log(string.Format("LOG: Infra frame saved to file {0}", path));
                }
                return true;
            }
            catch (Exception ex)
            {
                ServiceProvider.Log(ex.Message);
                return false;
            }
        }

        private void SaveIrData(IntPtr intPtr, uint bufferLength, string path)
        {
            int width = _irFrameDescription.Width;
            int height = _irFrameDescription.Height;
            var data = new short[width * height];
            Marshal.Copy(intPtr, data, 0, width * height);
            using (var sw = new StreamWriter(path, false))
            {
                sw.Write(string.Join(",", data));
            }
        }


        private unsafe void SaveDepthBitmap(IntPtr intPtr, uint bufferLength, ushort minDepth, ushort maxDepth, string fileName)
        {
            int width = _depthFrameDescription.Width;
            int height = _depthFrameDescription.Height;
            PixelFormat format = PixelFormats.Bgra32;
            byte[] pixels = new byte[_depthFrameDescription.Width * _depthFrameDescription.Height *
                    (format.BitsPerPixel + 7) / 8];
            ushort* depthData = (ushort*)intPtr;
            int colorIndex = 0;
            for (int bufferIndex = 0; bufferIndex < (int)(bufferLength / _depthFrameDescription.BytesPerPixel); ++bufferIndex)
            {
                ushort depth = depthData[bufferIndex];
                byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? depth / MapDepthToByte : 0);
                pixels[colorIndex++] = intensity; // Blue
                pixels[colorIndex++] = intensity; // Green
                pixels[colorIndex++] = intensity; // Red

                ++colorIndex;
            }

            fixed (byte* ptr = pixels)
            {
                int stride = width * format.BitsPerPixel / 8;
                using (Bitmap image = new Bitmap(width, height, stride,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb, new IntPtr(ptr)))
                {

                    image.Save(fileName);
                }
            }
        }

        private unsafe void SaveDepthData(IntPtr intPtr, uint bufferLength, string fileName)
        {
            int width = _depthFrameDescription.Width;
            int height = _depthFrameDescription.Height;
            var data = new short[width * height];
            Marshal.Copy(intPtr, data, 0, width * height);
            using (var sw = new StreamWriter(fileName, false))
            {
                sw.Write(string.Join(",", data));
            }
        }

        /// <summary>
        /// Save Ir Bitmap
        /// </summary>
        /// <param name="intPtr"></param>
        /// <param name="bufferLength"></param>
        /// <param name="irOutputMax"></param>
        /// <param name="irSourceMax"></param>
        /// <param name="irSourceScale"></param>
        /// <param name="irOutMin"></param>
        /// <param name="p"></param>
        private unsafe void SaveIrBitmap(IntPtr intPtr, uint bufferLength, float irOutputMax, float irSourceMax, float irSourceScale, float irOutMin, string fileName)
        {
            int width = _irFrameDescription.Width;
            int height = _irFrameDescription.Height;
            PixelFormat format = PixelFormats.Bgra32;
            byte[] pixels = new byte[_irFrameDescription.Width * _irFrameDescription.Height *
                    (format.BitsPerPixel + 7) / 8];
            ushort* data = (ushort*)intPtr;
            int colorIndex = 0;
            for (int bufferIndex = 0; bufferIndex < (int)(bufferLength / _irFrameDescription.BytesPerPixel); ++bufferIndex)
            {
                ushort ir = data[bufferIndex];
                //byte intensity = (byte)(ir >= minDepth && ir <= maxDepth ? ir / MapDepthToByte : 0);
                //byte intensity = (byte)(Math.Min(_irOutputMax, (((ir / _irSourceMax * _irSourceScale) * (1.0f - _irOutMin)) + _irOutMin)) * 256);
                byte intensity = (byte)(Math.Min(irOutputMax, ir / irSourceMax * irSourceScale) * 256);
                pixels[colorIndex++] = intensity; // Blue
                pixels[colorIndex++] = intensity; // Green
                pixels[colorIndex++] = intensity; // Red

                ++colorIndex;
            }

            fixed (byte* ptr = pixels)
            {
                int stride = width * format.BitsPerPixel / 8;
                using (Bitmap image = new Bitmap(width, height, stride,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb, new IntPtr(ptr)))
                {
                    image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    image.Save(fileName);
                }
            }


        }

        private unsafe void DisplayIrBitmap(IntPtr intPtr, uint bufferLength, float irOutputMax, float irSourceMax, float irSourceScale, float irOutMin)
        {
            int width = _irFrameDescription.Width;
            int height = _irFrameDescription.Height;
            PixelFormat format = PixelFormats.Bgra32;
            byte[] pixels = new byte[_irFrameDescription.Width * _irFrameDescription.Height *
                    (format.BitsPerPixel + 7) / 8];
            ushort* data = (ushort*)intPtr;
            _realtimeImage.Lock();
            byte* bufferPtr = (byte*)_realtimeImage.BackBuffer;
            for (int bufferIndex = 0; bufferIndex < (int)(bufferLength / _irFrameDescription.BytesPerPixel); ++bufferIndex)
            {
                ushort ir = data[bufferIndex];
                //byte intensity = (byte)(ir >= minDepth && ir <= maxDepth ? ir / MapDepthToByte : 0);
                //byte intensity = (byte)(Math.Min(_irOutputMax, (((ir / _irSourceMax * _irSourceScale) * (1.0f - _irOutMin)) + _irOutMin)) * 256);
                byte intensity = (byte)(Math.Min(irOutputMax, ir / irSourceMax * irSourceScale) * 256);
                *bufferPtr++ = intensity;
                *bufferPtr++ = intensity;
                *bufferPtr++ = intensity;
                *bufferPtr++ = 255;
            }
            _realtimeImage.AddDirtyRect(new Int32Rect(0, 0, width, height));
            _realtimeImage.Unlock();
        }

        void _kinectSensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            if (e.IsAvailable)
            {
                ServiceProvider.Log("LOG: Kinect come online.");
                KinectConnected(this, new EventArgs());
            }
            else
            {
                ServiceProvider.Log(
                    "ERROR: KinectSensor unpluged, please solve this problem and start the application again.");

                KinectDisconnected(this, new EventArgs());
            }
        }

        public static bool IsConnected
        {
            get
            {
                return Instance._kinectSensor.IsAvailable;
            }
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                //this._colorFrameReader.FrameArrived -= ColorFrameArrived;
                Marshal.FreeHGlobal(_a);
                Marshal.FreeHGlobal(_b);
                Marshal.FreeHGlobal(_c);

                Marshal.FreeHGlobal(_aCopy);
                Marshal.FreeHGlobal(_bCopy);
                Marshal.FreeHGlobal(_cCopy);
                _isDisposed = true;
            }
        }

        ~KinectHelper()
        {
            this.Dispose();
        }
    }
}