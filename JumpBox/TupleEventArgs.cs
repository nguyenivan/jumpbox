﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JumpBox
{
    public class TupleEventArgs:EventArgs
    {
        public Tuple<string, string> FileTuple { get; set; }
        public TupleEventArgs(Tuple<string, string> fileTuple)
        {
            this.FileTuple = fileTuple;
        }
    }
}
