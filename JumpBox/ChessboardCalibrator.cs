﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace JumpBox
{

    [StructLayout(LayoutKind.Sequential)]
    public struct CalibrationSet
    {
        public String KinectIrImage;
        public String CameraImage;
        public String DepthData;
        public CalibrationSet(String kinectIrImage, String cameraImage, String depthData)
        {
            KinectIrImage = kinectIrImage;
            CameraImage = cameraImage;
            DepthData = depthData;
        }
    }
}
