﻿using CameraControl.Devices;
using CameraControl.Devices.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace JumpBox
{



    /// <summary>
    /// Interaction logic for Calibration.xaml
    /// </summary>
    public partial class Calibration : Window, ILog, INotifyPropertyChanged
    {

        ICameraDevice _selectedCam = null;

        [DllImport("Calibration.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern bool calibrate(CalibrationSet[] calibrationSet, int size, String outputFile);

        public Calibration()
        {

            InitializeComponent();
            TextBoxUtilities.SetAlwaysScrollToEnd(LogBox, true);
            DataContext = this;
            CaptureRepeat = 30;
            CaptureInterval = 5;
            _timer.Tick += timer_Tick;


            ServiceProvider.CameraDeviceManager.CameraConnected += CameraDeviceManager_CameraConnected;
            ServiceProvider.CameraDeviceManager.CameraDisconnected += CameraDeviceManager_CameraDisconnected;
            KinectHelper.KinectConnected += KinectHelper_KinectConnected;
            KinectHelper.KinectDisconnected += KinectHelper_KinectDisconnected;
            ServiceProvider.StorageHelper.OnNewCalibration += StorageHelper_OnNewCalibration;
            ServiceProvider.StorageHelper.InitializeCalibrationList();
            CameraLed.TurnedOn = ServiceProvider.CameraDeviceManager.ConnectToCamera();
            //KinectLed.TurnedOn = KinectHelper.IsConnected;
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public ImageSource KinectImage
        {
            get
            {
                return KinectHelper.ImageSource;
            }
        }

        void StorageHelper_OnNewCalibration(object sender, TupleEventArgs e)
        {
            AddNewCalibrationTuple(e.FileTuple);
        }

        private void KinectHelper_KinectDisconnected(object sender, EventArgs e)
        {
            this.KinectLed.TurnedOn = false;
        }

        private void KinectHelper_KinectConnected(object sender, EventArgs e)
        {
            this.KinectLed.TurnedOn = true;
        }

        void CameraDeviceManager_CameraDisconnected(CameraControl.Devices.ICameraDevice cameraDevice)
        {
            if (_selectedCam == cameraDevice) _selectedCam = null;
            this.Dispatcher.BeginInvoke(new Action(() => this.CameraLed.TurnedOn = false));
        }

        void CameraDeviceManager_CameraConnected(CameraControl.Devices.ICameraDevice cameraDevice)
        {
            _selectedCam = cameraDevice;
            if (_selectedCam != null)
            {
                _selectedCam.PhotoCaptured += SelectedCam_PhotoCaptured;
                _selectedCam.CaptureCompleted += SelectedCam_CaptureCompleted;
            }
            this.Dispatcher.BeginInvoke(new Action(() => this.CameraLed.TurnedOn = true));
        }

        private void SelectedCam_CaptureCompleted(object sender, EventArgs e)
        {
        }

        private void SelectedCam_PhotoCaptured(object sender, CameraControl.Devices.Classes.PhotoCapturedEventArgs eventArgs)
        {
            try
            {
                if (!this.AutoCapture)
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show("Is this a background picture?", "JumpBox Calibration Message", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.Cancel:
                            return;
                        case MessageBoxResult.Yes:
                            _capturedEventArgs = eventArgs;
                            _selectedCam.IsBusy = true;
                            _selectedCam.TransferFile(_capturedEventArgs.Handle, ServiceProvider.StorageHelper.BackgroundFilePath);
                            return;
                        case MessageBoxResult.No:
                            break;
                    }
                }
                _capturedEventArgs = eventArgs;
                Tuple<string, string, string, string> fileTuple = ServiceProvider.StorageHelper.GetNextFileTuple<Tuple<string, string, string, string>>(
                    new Tuple<string, string, string, string>("camera", "infra", "depth", "bindex"),
                    new Tuple<string, string, string, string>("jpg", "jpg", "csv", "csv")
                 );
                _selectedCam.IsBusy = true;
                _selectedCam.TransferFile(_capturedEventArgs.Handle, fileTuple.Item1);
                KinectHelper.SaveIrFrame(fileTuple.Item2);
                KinectHelper.SaveDepthFrame(fileTuple.Item3);
                KinectHelper.SaveBodyIndexFrame(fileTuple.Item4);
            }
            finally
            {
                _selectedCam.IsBusy = false;
            }
        }

        public bool AutoCapture
        {
            get { return _autoCapture; }
            set
            {
                _autoCapture = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AutoCapture"));
            }
        }


        public int CaptureRepeat
        {
            get { return _captureRepeat; }
            set
            {
                _captureRepeat = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CaptureRepeat"));
            }
        }

        public int CaptureInterval
        {
            get { return _captureInterval; }
            set
            {
                _captureInterval = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CaptureInterval"));
            }
        }

        public void Log(string message)
        {
            this.Dispatcher.BeginInvoke(new Action(() => LogBox.AppendText(message + Environment.NewLine)));
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private bool _autoCapture;
        private int _captureRepeat;
        private int _captureInterval;
        private PhotoCapturedEventArgs _capturedEventArgs;
        private int _tickCount = 0;
        private DispatcherTimer _timer = new DispatcherTimer();


        private void ButtonSpinner_Spin(object sender, Xceed.Wpf.Toolkit.SpinEventArgs e)
        {
            ButtonSpinner spinner = (ButtonSpinner)sender;
            TextBox txtBox = (TextBox)spinner.Content;

            int value = String.IsNullOrEmpty(txtBox.Text) ? 0 : Convert.ToInt32(txtBox.Text);
            if (e.Direction == SpinDirection.Increase)
                value++;
            else
                value--;
            txtBox.Text = value.ToString();
        }

        private void AddNewCalibrationTuple(Tuple<string, string> fileTuple)
        {
            var newRow = new RowDefinition();
            newRow.Height = new GridLength(64);
            var kIm = fileTuple.Item1;
            var cIm = fileTuple.Item2;

            BitmapImage image;

            image= new BitmapImage();
            image.DecodePixelWidth = 64;
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(string.Format("pack://siteoforigin:,,/{0}", kIm));
            image.EndInit();
            
            var kImCtrl = new Image();
            kImCtrl.Source = image;

            image = new BitmapImage();
            image.DecodePixelWidth = 64;
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(string.Format("pack://siteoforigin:,,/{0}", cIm));
            image.EndInit();

            var cImCtrl = new Image();
            cImCtrl.Source = image;



            var currentRow = this.Preview.RowDefinitions.Count;

            Grid.SetColumn(kImCtrl, 0);
            Grid.SetRow(kImCtrl, currentRow);
            Grid.SetColumn(cImCtrl, 1);
            Grid.SetRow(cImCtrl, currentRow);

            this.Preview.RowDefinitions.Add(newRow);
            this.Preview.Children.Add(kImCtrl);
            this.Preview.Children.Add(cImCtrl);
        }

        private void Capture_Click(object sender, RoutedEventArgs e)
        {
            if (!KinectHelper.IsConnected || _selectedCam == null)
            {
                ServiceProvider.Log("Please connect both Kinect and Camera to begin capture");
                return;
            }
            if (this.AutoCapture)
            {
                _tickCount = 0;
                _timer.Interval = TimeSpan.FromSeconds(this.CaptureInterval);
                _timer.Start();
            }
            else
            {
                Thread threadcamera = new Thread(new ThreadStart(delegate
                {
                    _selectedCam.CapturePhoto();
                }));
                threadcamera.Start();
                KinectHelper.Capture();
            }
        }

        private void Calibrate_Click(object sender, RoutedEventArgs e)
        {
            CalibrationSet[] sample = new CalibrationSet[]{
	            new CalibrationSet("../../Resources/Dataset 4.1/infra002.jpg","../../Resources/Dataset 4.1/camera002.jpg","../../Resources/Dataset 4.1/depth002.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra003.jpg","../../Resources/Dataset 4.1/camera003.jpg","../../Resources/Dataset 4.1/depth003.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra004.jpg","../../Resources/Dataset 4.1/camera004.jpg","../../Resources/Dataset 4.1/depth004.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra005.jpg","../../Resources/Dataset 4.1/camera005.jpg","../../Resources/Dataset 4.1/depth005.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra006.jpg","../../Resources/Dataset 4.1/camera006.jpg","../../Resources/Dataset 4.1/depth006.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra007.jpg","../../Resources/Dataset 4.1/camera007.jpg","../../Resources/Dataset 4.1/depth007.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra008.jpg","../../Resources/Dataset 4.1/camera008.jpg","../../Resources/Dataset 4.1/depth008.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra009.jpg","../../Resources/Dataset 4.1/camera009.jpg","../../Resources/Dataset 4.1/depth009.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra010.jpg","../../Resources/Dataset 4.1/camera010.jpg","../../Resources/Dataset 4.1/depth010.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra011.jpg","../../Resources/Dataset 4.1/camera011.jpg","../../Resources/Dataset 4.1/depth011.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra012.jpg","../../Resources/Dataset 4.1/camera012.jpg","../../Resources/Dataset 4.1/depth012.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra013.jpg","../../Resources/Dataset 4.1/camera013.jpg","../../Resources/Dataset 4.1/depth013.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra014.jpg","../../Resources/Dataset 4.1/camera014.jpg","../../Resources/Dataset 4.1/depth014.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra015.jpg","../../Resources/Dataset 4.1/camera015.jpg","../../Resources/Dataset 4.1/depth015.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra016.jpg","../../Resources/Dataset 4.1/camera016.jpg","../../Resources/Dataset 4.1/depth016.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra017.jpg","../../Resources/Dataset 4.1/camera017.jpg","../../Resources/Dataset 4.1/depth017.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra018.jpg","../../Resources/Dataset 4.1/camera018.jpg","../../Resources/Dataset 4.1/depth018.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra019.jpg","../../Resources/Dataset 4.1/camera019.jpg","../../Resources/Dataset 4.1/depth019.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra020.jpg","../../Resources/Dataset 4.1/camera020.jpg","../../Resources/Dataset 4.1/depth020.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra021.jpg","../../Resources/Dataset 4.1/camera021.jpg","../../Resources/Dataset 4.1/depth021.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra022.jpg","../../Resources/Dataset 4.1/camera022.jpg","../../Resources/Dataset 4.1/depth022.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra023.jpg","../../Resources/Dataset 4.1/camera023.jpg","../../Resources/Dataset 4.1/depth023.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra024.jpg","../../Resources/Dataset 4.1/camera024.jpg","../../Resources/Dataset 4.1/depth024.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra025.jpg","../../Resources/Dataset 4.1/camera025.jpg","../../Resources/Dataset 4.1/depth025.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra026.jpg","../../Resources/Dataset 4.1/camera026.jpg","../../Resources/Dataset 4.1/depth026.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra027.jpg","../../Resources/Dataset 4.1/camera027.jpg","../../Resources/Dataset 4.1/depth027.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra028.jpg","../../Resources/Dataset 4.1/camera028.jpg","../../Resources/Dataset 4.1/depth028.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra029.jpg","../../Resources/Dataset 4.1/camera029.jpg","../../Resources/Dataset 4.1/depth029.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra030.jpg","../../Resources/Dataset 4.1/camera030.jpg","../../Resources/Dataset 4.1/depth030.csv") ,
	            new CalibrationSet("../../Resources/Dataset 4.1/infra031.jpg","../../Resources/Dataset 4.1/camera031.jpg","../../Resources/Dataset 4.1/depth031.csv")
            };
            calibrate(sample, sample.Length, "calibration.yaml");
            this.Close();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Thread threadcamera = new Thread(new ThreadStart(delegate
            {
                _selectedCam.CapturePhoto();
            }));
            threadcamera.Start();
            KinectHelper.Capture();
            if (++_tickCount >= CaptureRepeat)
            {
                _timer.Stop();
            }

        }

        private void ClearFolder_Click(object sender, RoutedEventArgs e)
        {
            var directory = new DirectoryInfo(ServiceProvider.StorageHelper.CalibrationFolder);
            foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
            this.Preview.Children.Clear();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Capture capture = new Capture();
            capture.Show();
            ServiceProvider.AddLogProvider(capture);
        }
    }
}
