#ifdef  CALIBRATION_EXPORTS 
#define DLLEXPORT __declspec(dllexport)  
#else
#define DLLEXPORT __declspec(dllimport)  
#endif


#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

typedef struct DLLEXPORT _CalibrationSet{
	const char* kinectIrImage;
	const char* cameraImage;
	const char* depthData;
} CalibrationSet;

typedef struct DLLEXPORT _SizeInt{
	int width;
	int height;
	inline _SizeInt(int w, int h) {
		width = w;
		height = h;
	}
	inline operator CvSize(){
		return cvSize(width, height);
	}
	inline operator Size(){
		return Size(width, height);
	}
} SizeInt;


class DLLEXPORT ChessboardCalibrator
{
private:
	const int MAX_PLANE = 3000;
	const int MIN_PLANE = 1500;
	const int KINECT_WIDTH = 512;
	const int KINECT_HEIGHT = 424;
	const int DILATE_AMT = 50;
	const int ERODE_AMT = 10;
	const int BLUR_AMT = 20;
	const int BLUR_PRE = 20;

	template <typename Container>
	struct compare_indirect_index
	{
		const Container& container;
		compare_indirect_index(const Container& container) : container(container) { }
		bool operator () (size_t lindex, size_t rindex) const
		{
			return container[lindex] < container[rindex];
		}
	};

public:
	const static int THRESH_NEAR = 2000;
	const static int THRESH_FAR = 4000;

	ChessboardCalibrator();
	virtual ~ChessboardCalibrator();
	vector<Point3f> getWorldObjectPoints(
		InputArray kinectMatrix,
		InputArray kinectDist,
		vector<Point2f> kinectCorners,
		vector<Point2f> cameraCorners,
		vector<ushort> depthData
		);

	void ChessboardCalibrator::depthRemoval(
		vector<ushort> rawDepth,
		vector<ushort> bodyIndices,
		int depthWidth,
		int depthHeight,
		Mat colorIm,
		Mat depthMatrix,
		Mat depthDist,
		Mat colorRVec,
		Mat colorTVec,
		Mat colorMatrix,
		Mat colorDist,
		OutputArray mask,
		int nearClipPlane = THRESH_NEAR, // in mm
		int farClipPlane = THRESH_FAR // in mm
		);

	bool filterCombine(
		const char* calibrationConfig,
		const char* backgroundImage,
		const char* cameraImage,
		const char* depthData,
		const char* outputImage,
		const char* bodyIndex,
		int nearClipPlane = THRESH_NEAR, // in mm
		int farClipPlane = THRESH_FAR // in mm
	);

	bool extractCorners(
		String filePath,
		vector<Point2f>& imageCorners,
		Size& size
	);
	void readDepthCsv(String fileName, vector<ushort>& vect);

	Point3f ChessboardCalibrator::depthScreenToWorld(float x, float y, float z, InputArray camMatrix, InputArray kinectDist);
	bool ChessboardCalibrator::singleCalibrate(
		CalibrationSet calibrationSet,
		vector<Point2f>& kinectCorners,
		vector<Point2f>& cameraCorners,
		vector<ushort>& depthSpaceObjectPoints,
		Size& kinectSize,
		Size& cameraSize
		);

	void ChessboardCalibrator::getCornersAndObjectPoints(
		vector<CalibrationSet> calibrationData,
		vector<Point3f> baseObjectPoints,
		vector<vector<Point2f>>& kinectPoints,
		vector<vector<Point2f>>& cameraPoints,
		vector<vector<ushort>>& depthPoints,
		vector<vector<Point3f>>& objectPoints,
		Size& kinectSize,
		Size& cameraSize
		);

	bool ChessboardCalibrator::calibrate(
		vector<CalibrationSet> calibrationData,
		OutputArray kinectMatrix,
		OutputArray kinectDist,
		OutputArray cameraMatrix,
		OutputArray cameraDist,
		OutputArray cameraRVec,
		OutputArray cameraTVec,
		OutputArray cameraInliers
		);

	vector<Point3f> ChessboardCalibrator::initObjectPoints(Size size, float squareSize = 1.0f) const;

	SizeInt BOARD_SIZE = SizeInt(10, 7);

	void ChessboardCalibrator::getWorldPoints(
		vector<vector<Point2f>>kinectPoints,
		vector<vector<ushort>> depthPoints,
		InputArray kinectMatrix,
		InputArray kinectDist,
		vector<vector<Point3f>>& worldPoints
		);
	
	//void ChessboardCalibrator::StartCounter();
	//double ChessboardCalibrator::GetCounter();
};


extern "C" DLLEXPORT bool calibrate(CalibrationSet* data, int size, const char* outputFile);

extern "C" DLLEXPORT bool filter(
	const char* calibrationConfig,
	const char* backgroundImage,
	const char* cameraImage,
	const char* depthData,
	const char* outputImage,
	const char* bodyIndex,
	int nearClipPlane = ChessboardCalibrator::THRESH_NEAR, // in mm
	int farClipPlane = ChessboardCalibrator::THRESH_FAR // in mm
	);