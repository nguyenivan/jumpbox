#include "stdafx.h"
#include "ChessboardCalibrator.h"
#include "fstream"
#include "Windows.h"
#include "ChessboardUtils.h"
#include <numeric>


ChessboardCalibrator::ChessboardCalibrator()
{
}

ChessboardCalibrator::~ChessboardCalibrator()
{
}

vector<Point3f> ChessboardCalibrator::getWorldObjectPoints(
	InputArray kinectMatrix,
	InputArray kinectDist,
	vector<Point2f> kinectCorners,
	vector<Point2f> cameraCorners,
	vector<ushort> depthData
	)
{
	return vector<Point3f>();
}

void ChessboardCalibrator::readDepthCsv(String fileName, vector<ushort>& vect) {

	std::ifstream t(fileName);
	std::string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());

	std::stringstream ss(str);
	ushort i;
	while (ss >> i)
	{
		vect.push_back(i);
		if (ss.peek() == ',' || ss.peek() == ' ')
			ss.ignore();
	}
}

bool ChessboardCalibrator::extractCorners(
	String filePath,
	vector<Point2f>& imageCorners,
	Size& size
	){
	double min = 256, max = 1024;
	Mat image;
	if (ChessboardUtils::endsWith(filePath, ".csv")) {
		vector<ushort> irData; readDepthCsv(filePath, irData);
		/*vector<unsigned char> greyData;
		for (auto u : irData) {
			unsigned char  c;
			ushort uc = u - min;
			uc *= 255 / (max - min);
			c = (unsigned char)(std::min((unsigned short)255, uc));
			assert(c <= 255 && c >= 0);
			greyData.push_back(c);
		}*/
		image = Mat(KINECT_HEIGHT, KINECT_WIDTH, CV_8U);
		for (int i = 0; i < KINECT_HEIGHT; i++) {
			for (int j = 0; j < KINECT_WIDTH; j++){
				// NEED TO FLIP HERE
				ushort uc = irData.at(i*KINECT_WIDTH  + (KINECT_WIDTH - j - 1) );

				if (uc>min) {
					uc =ushort(uc- min);
				}
				else {
					uc = 0;
				}
				uc = ushort(uc* 255 / (max - min));
				uc = std::min(uc, ushort(255));
				
				unsigned char  c = (unsigned char)(uc);
				image.at<uchar>(i, j) = c;
			}
		}
		//image = Mat(KINECT_HEIGHT, KINECT_WIDTH, CV_8UC1, greyData.data(), CV_AUTO_STEP);
		//image -= min;
		//image.convertTo(image, CV_8U, 255.0 / (max - min));
		imshow("Test", image);
		waitKey(10);
	}
	else {
		image = imread(filePath.c_str(), CV_LOAD_IMAGE_GRAYSCALE);
	}

	size = image.size();

	bool found = findChessboardCorners(image, BOARD_SIZE, imageCorners, 0);

	//if (found) {
	//	cornerSubPix(image, imageCorners, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
	//}
	cout << min << "," << max << ":" << (found ? "OK" : "NO") << endl;
	return found;
}

Point3f ChessboardCalibrator::depthScreenToWorld(float x, float y, float z, InputArray camMatrix, InputArray kinectDist) {
	float fx = (float)camMatrix.getMat().at<double>(0, 0);
	float fy = (float)camMatrix.getMat().at<double>(1, 1);
	float cx = (float)camMatrix.getMat().at<double>(0, 2);
	float cy = (float)camMatrix.getMat().at<double>(1, 2); // Can be optimzed
	return Point3f((x - cx) * z / fx, (float)(y - cy) * z / fy, z);
}

bool ChessboardCalibrator::singleCalibrate(
	CalibrationSet calibrationSet,
	vector<Point2f>& kinectCorners,
	vector<Point2f>& cameraCorners,
	vector<ushort>& depthSpaceObjectPoints,
	Size& kinectSize,
	Size& cameraSize
	)
{
	ChessboardUtils::StartCounter();
	double lastProcess = 0.0;

	cout << calibrationSet.kinectIrImage << " ... ";
	bool foundKinect = extractCorners(calibrationSet.kinectIrImage, kinectCorners, kinectSize);
	cout << (!foundKinect ? "NOT Found" : "FOUND") << " ";
	lastProcess = ChessboardUtils::GetCounter() - lastProcess;
	cout << lastProcess << " seconds\n";

	cout << calibrationSet.cameraImage << " ... ";
	bool foundCamera = extractCorners(calibrationSet.cameraImage, cameraCorners, cameraSize);
	cout << (!foundCamera ? "NOT Found" : "FOUND") << " ";
	lastProcess = ChessboardUtils::GetCounter() - lastProcess;
	cout << lastProcess << " seconds\n";

	readDepthCsv(calibrationSet.depthData, depthSpaceObjectPoints);

	return foundKinect && foundCamera;
}


void ChessboardCalibrator::getCornersAndObjectPoints(
	vector<CalibrationSet> calibrationData,
	vector<Point3f> baseObjectPoints,
	vector<vector<Point2f>>& kinectPoints,
	vector<vector<Point2f>>& cameraPoints,
	vector<vector<ushort>>& depthPoints,
	vector<vector<Point3f>>& objectPoints,
	Size& kinectSize,
	Size& cameraSize
	)
{
	int count = 0;
	for (auto sample : calibrationData) {
		vector<Point2f> kinectCorners, cameraCorners;
		vector<ushort> depthData;
		Size singleKinectSize;
		Size singleCameraSize;
		bool found = singleCalibrate(sample, kinectCorners, cameraCorners, depthData, singleKinectSize, singleCameraSize);

		if (found){

			assert(++count || (kinectSize == singleKinectSize   && cameraSize == singleCameraSize)); // check if all images equal in size size 

			if (count == 1) {
				kinectSize = singleKinectSize;
				cameraSize = singleCameraSize;
			}

			//Mat im1 = imread(sample.cameraImage);
			//drawChessboardCorners(im1, BOARD_SIZE, cameraCorners, true);

			//Mat im2 = imread(sample.kinectIrImage);
			//drawChessboardCorners(im2, BOARD_SIZE, kinectCorners, true);

			//Size sz1 = im1.size();
			//Size sz2 = im2.size();
			//Mat im3(sz1.height, sz1.width + sz2.width, CV_8UC3);
			//Mat left(im3, Rect(0, 0, sz1.width, sz1.height));
			//im1.copyTo(left);
			//Mat right(im3, Rect(sz1.width, 0, sz2.width, sz2.height));
			//im2.copyTo(right);
			//String windowsName = "Processed Image";
			//namedWindow(windowsName, CV_WINDOW_NORMAL);
			//imshow(windowsName, im2);

			//Size screenSize(1280, 720);
			//Size imageSize = im3.size();

			//float scale = fmin((float)(screenSize.width / imageSize.width), (float)(screenSize.height / imageSize.height));
			//scale = fmax(scale, 1.0f);
			//cv::resizeWindow(windowsName, int(scale * imageSize.width), int(scale * imageSize.height));
			//waitKey(2000);

			kinectPoints.push_back(kinectCorners);
			cameraPoints.push_back(cameraCorners);
			depthPoints.push_back(depthData);
			objectPoints.push_back(baseObjectPoints);

		}
	}
	//waitKey();
	//cv::destroyAllWindows();


}


bool ChessboardCalibrator::calibrate(
	vector<CalibrationSet> calibrationData,
	OutputArray kinectMatrix,
	OutputArray kinectDist,
	OutputArray cameraMatrix,
	OutputArray cameraDist,
	OutputArray cameraRVec,
	OutputArray cameraTVec,
	OutputArray cameraInliers
	)
{
	// Canonical object points like (1,0,0) ...
	vector<Point3f> baseObjectPoints = initObjectPoints(BOARD_SIZE);

	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> kinectPoints;
	vector<vector<Point2f>> cameraPoints;
	vector<vector<ushort>> depthPoints;
	Size kinectSize;
	Size cameraSize;

	getCornersAndObjectPoints(calibrationData, baseObjectPoints,
		kinectPoints, cameraPoints, depthPoints, objectPoints, kinectSize, cameraSize);

	vector<Mat> RVecs;
	vector<Mat> TVecs;

	// Calibrate to find kinect matrix
	calibrateCamera(objectPoints, kinectPoints, kinectSize, kinectMatrix, kinectDist, RVecs, TVecs);

	// Calibrate to find camera matrix
	calibrateCamera(objectPoints, cameraPoints, cameraSize, cameraMatrix, cameraDist, RVecs, TVecs);

	// Get new object points in world coordinate
	vector<vector<Point3f>> worldPoints;
	getWorldPoints(kinectPoints, depthPoints, kinectMatrix, kinectDist, worldPoints);

	vector<Point3f> newWorldPoints;
	for (auto v : worldPoints) {
		newWorldPoints.insert(end(newWorldPoints), begin(v), end(v));
	}

	vector<Point2f> newCameraPoints;
	for (auto v : cameraPoints) {
		newCameraPoints.insert(end(newCameraPoints), begin(v), end(v));
	}

	//Now calculate Rvec and Tvec or camera against kinect depth world
	solvePnPRansac(newWorldPoints, newCameraPoints, cameraMatrix, cameraDist, cameraRVec, cameraTVec, false, 100, 8.0f, 100, cameraInliers);
	return true;
}

// squareSize is in milimeter
vector<Point3f> ChessboardCalibrator::initObjectPoints(Size size, float squareSize) const
{
	vector<Point3f> obj;
	for (int j = 0; j < size.width * size.height; j++)
		obj.push_back(Point3f((j / size.width) * squareSize, (j%size.width) * squareSize, 0.0f));
	return obj;
}

void ChessboardCalibrator::getWorldPoints(
	vector<vector<Point2f>>kinectPoints,
	vector<vector<ushort>> depthPoints,
	InputArray kinectMatrix,
	InputArray kinectDist,
	vector<vector<Point3f>>& worldPoints
	){
	// Calculate new object points for Camera solve PnP
	vector<vector<ushort>>::iterator depth_it1 = depthPoints.begin();
	for (auto v : kinectPoints) {
		//vector<ushort>::iterator depth_it2 = depth_it1->begin();
		vector<Point3f> worldPointsRow;
		for (auto c : v){
			int x = int(c.x);
			int y = int(c.y);
			// Flip
			unsigned i = unsigned((y * KINECT_WIDTH) + (KINECT_WIDTH - x));
			//float z = *depth_it2;
			float z = depth_it1->at(i);
			/// Why we don't flip x here??? Double check
			Point3f worldPoint3f = depthScreenToWorld((float)x, (float)y, z, kinectMatrix, kinectDist);
			worldPointsRow.push_back(worldPoint3f);
			//++depth_it2;
		}
		worldPoints.push_back(worldPointsRow);
		++depth_it1;
	}
}

bool calibrate(CalibrationSet* data, int size, const char* outputFile){
	ChessboardCalibrator calib;

	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> kinectPoints;
	vector<vector<Point2f>> cameraPoints;
	vector<vector<ushort>> depthPoints;
	Size kinectSize;
	Size cameraSize;
	vector<CalibrationSet> sample(data, data + size);
	Mat kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers;


	calib.calibrate(sample, kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers);

	//Write calib data to corners.yaml file
	FileStorage save(outputFile, FileStorage::WRITE);
	save << "kinectMatrix" << kinectMatrix;
	save << "kinectDist" << kinectDist;
	save << "cameraMatrix" << cameraMatrix;
	save << "cameraDist" << cameraDist;
	save << "cameraRVec" << cameraRVec;
	save << "cameraTVec" << cameraTVec;
	save << "cameraInliers" << cameraInliers;
	save.release();

	return true;
}

bool filter(
	const char* calibrationConfig,
	const char* backgroundImage,
	const char* cameraImage,
	const char* depthData,
	const char* outputImage,
	const char* bodyIndex,
	int nearClipPlane, // in mm
	int farClipPlane // in mm
	) {
	ChessboardCalibrator calib;
	bool success = calib.filterCombine(
		calibrationConfig,
		backgroundImage,
		cameraImage,
		depthData,
		outputImage,
		bodyIndex,
		nearClipPlane,
		farClipPlane
		);
	return success;
}

void bitwiseRemoval(
	Mat backgroundImage,
	Mat objectImage,
	OutputArray mask
	) {

	// Check if channels == 1 , e.g grayscale
	Mat backgroundGray;
	if (backgroundGray.channels() == 3) {
		cvtColor(backgroundImage, backgroundGray, CV_BGR2GRAY);
	}
	else {
		backgroundGray = backgroundImage;
	}
	Mat objectGray;
	if (objectImage.channels() == 3) {
		cvtColor(objectImage, objectGray, CV_BGR2GRAY);
	}
	else {
		objectGray = objectImage;
	}
	absdiff(backgroundGray, objectGray, mask);
	int diffThreshold = 50;
	int maxValue = 255;
	threshold(mask, mask, diffThreshold, maxValue, CV_THRESH_BINARY);
}

void ChessboardCalibrator::depthRemoval(
	vector<ushort> rawDepth,
	vector<ushort> bodyIndices,
	int depthWidth,
	int depthHeight,
	Mat colorIm,
	Mat depthMatrix,
	Mat depthDist,
	Mat colorRVec,
	Mat colorTVec,
	Mat colorMatrix,
	Mat colorDist,
	OutputArray mask,
	int nearClipPlane, // in mm
	int farClipPlane// in mm
	)
{
	Mat depthMap = Mat(depthHeight, depthWidth, CV_16U, &rawDepth.front());

	double min, max;
	minMaxIdx(rawDepth, &min, &max);
	float scaleFactor = float(255 / max);
	double threshNear = nearClipPlane * scaleFactor;
	double threshFar = farClipPlane * scaleFactor;
	Mat depthShow; depthMap.convertTo(depthShow, CV_8UC1, scaleFactor);

	Mat bodyMap = Mat(depthHeight, depthWidth, CV_16U, &bodyIndices.front());
	Mat bodyMask; bodyMap.convertTo(bodyMask, CV_8UC1); bodyMask = 255 - bodyMask;
	threshold(bodyMask, bodyMask, 200, 255, CV_THRESH_BINARY);

	//thresholding
	Mat tnear, tfar;
	depthShow.copyTo(tnear);
	depthShow.copyTo(tfar);
	threshold(tnear, tnear, threshNear, 255, CV_THRESH_TOZERO);
	threshold(tfar, tfar, threshFar, 255, CV_THRESH_TOZERO_INV);
	depthShow = tnear & tfar & bodyMask;//or cvAnd(tnear,tfar,depthShow,NULL); to join the two thresholded images

	vector<vector<Point>> contours;
	findContours(depthShow, contours, 0, 1);


	int numContours = int(contours.size());
	if (numContours == 0) return; // No contour found

	// compute bounding box and circle to exclude small blobs(non human) or do further filtering,etc.
	vector<vector<Point> > contours_poly(numContours);
	vector<Rect> boundRect(numContours);
	vector<Point2f> centers(numContours);
	vector<float> radii(numContours);
	for (int i = 0; i < numContours; i++){
		approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
		minEnclosingCircle(contours_poly[i], centers[i], radii[i]);
	}

	// find the biggest circle and pick it only
	vector <size_t> indices(radii.size(), 0);
	iota(indices.begin(), indices.end(), 0);  // found in <numeric>
	sort(indices.begin(), indices.end(), compare_indirect_index <decltype(radii)>(radii));
	int maxCntrIdx = int(indices.back());

	vector<Point3f> worldContour;
	float* lastZ = NULL;
	for (Point dp : contours_poly[maxCntrIdx]) {
		// Flip
		int i = int(dp.x + (dp.y * depthWidth));
		float z = rawDepth.at(i);
		if (z <nearClipPlane || z > farClipPlane) {
			if (lastZ == NULL) {
				z = (z<nearClipPlane) ? float(nearClipPlane): float(farClipPlane); // Value can be changed to the edge instead of the middle
				lastZ = new float;
			}
			else {
				z = *lastZ;
			}
		}
		//need to flip
		Point3f worldPoint = depthScreenToWorld(float(depthWidth - dp.x), float(dp.y), z, depthMatrix, depthDist);
		worldContour.push_back(worldPoint);
		lastZ = &z;
	}

	vector<Point2f> newContour;
	vector<vector<Point>> colorContours;
	projectPoints(worldContour, colorRVec, colorTVec, colorMatrix, colorDist, newContour);

	colorContours.push_back(vector<Point>());
	Mat(newContour).copyTo(colorContours[0]);

	mask.create(colorIm.size(), CV_8UC1);
	mask.getMat().setTo(Scalar(0));
	drawContours(mask, colorContours, 0, Scalar(255, 0, 0), -1, 8); // this is the mask

	//Mat cntr = Mat::zeros(colorIm.size(), CV_8UC1);
	//drawContours(cntr, colorContours, 0, Scalar(255, 0, 0), 10, 8);
	//ChessboardUtils::showResize("Ref", mask); waitKey();

}

bool ChessboardCalibrator::filterCombine(
	const char* calibrationConfig,
	const char* backgroundImage,
	const char* cameraImage,
	const char* depthData,
	const char* outputImage,
	const char* bodyIndex,
	int nearClipPlane, // in mm
	int farClipPlane // in mm
	){
	String calibFile = calibrationConfig;
	FileStorage calibStorage(calibFile, FileStorage::READ);
	Mat cameraMatrix, kinectMatrix, cameraDist, kinectDist, cameraRVec, cameraTVec;
	calibStorage["cameraMatrix"] >> cameraMatrix;
	calibStorage["cameraDist"] >> cameraDist;
	calibStorage["kinectMatrix"] >> kinectMatrix;
	calibStorage["kinectDist"] >> kinectDist;
	calibStorage["cameraRVec"] >> cameraRVec;
	calibStorage["cameraTVec"] >> cameraTVec;
	calibStorage.release();

	cout << kinectMatrix << endl;
	cout << cameraMatrix << endl;

	Mat backgroundImGray = imread(backgroundImage, 0);

	int threshNear = nearClipPlane;
	int threshFar = farClipPlane;
	int dilateAmt = DILATE_AMT;
	int erodeAmt = ERODE_AMT;
	int blurAmt = BLUR_AMT;
	int blurPre = BLUR_PRE;

	vector<vector<Point> > contours;
	namedWindow("Ref", CV_WINDOW_NORMAL);

	Size screenSize = backgroundImGray.size();

	Mat cameraIm = imread(cameraImage, CV_LOAD_IMAGE_COLOR);
	vector<ushort> depthPoints;
	readDepthCsv(depthData, depthPoints);
	vector<ushort> bodyIndices;
	readDepthCsv(bodyIndex, bodyIndices);

	Mat baseMask; depthRemoval(depthPoints,bodyIndices, KINECT_WIDTH, KINECT_HEIGHT, cameraIm, kinectMatrix, kinectDist, cameraRVec, cameraTVec, cameraMatrix, cameraDist, baseMask,nearClipPlane, farClipPlane);

	Mat minMask; erode(baseMask, minMask, Mat(), Point(-1, -1), erodeAmt);
	Mat roiMask; dilate(baseMask, roiMask, Mat(), Point(-1, -1), dilateAmt);
	Mat fineMask; bitwiseRemoval(backgroundImGray, cameraIm, fineMask);
	Mat cameraMask; bitwise_or(minMask, fineMask, cameraMask, roiMask);


	Mat show; cameraIm.copyTo(show, cameraMask); // Final image, which is masked

	ChessboardUtils::showResize("Ref", show);
	waitKey();

	imwrite(outputImage, show);
	destroyAllWindows();

	return true;
}
