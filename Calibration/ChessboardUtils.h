#include <iostream>
#include <Windows.h>

class ChessboardUtils
{
public:

	ChessboardUtils()
	{
	}

	static void showResize(const string& winname, InputArray im) {
		Size screenSize(1280, 720);
		Size imageSize = im.size();

		float scale = fmin((float)screenSize.width / imageSize.width, (float)screenSize.height / imageSize.height);
		cv::resizeWindow(winname, int(scale * imageSize.width), int(scale * imageSize.height));
		imshow("Ref", im);
	}

	static bool endsWith(const string& s, const string& suffix)
	{
		return s.size() >= suffix.size() && s.rfind(suffix) == (s.size() - suffix.size());
	}

	static void ChessboardUtils::StartCounter()
	{
		LARGE_INTEGER li;
		if (!QueryPerformanceFrequency(&li))
			std::cout<< "QueryPerformanceFrequency failed!\n";

		PCFreq = double(li.QuadPart);

		//Change this to use millisecons
		//PCFreq = double(li.QuadPart) / 1000.0;

		QueryPerformanceCounter(&li);
		CounterStart = li.QuadPart;
	}
	static double ChessboardUtils::GetCounter()
	{
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		return double(li.QuadPart - CounterStart) / PCFreq;
	}

	template <typename T>
	static void writeVectorOfVector(FileStorage &fs, string name, vector<vector<T>> &vectorOfVector)
	{
		fs << name;
		fs << "{";
		for (unsigned int i = 0; i < vectorOfVector.size(); i++)
		{
			fs << name + "_" + to_string(i);
			vector<T> tmp = vectorOfVector[i];
			fs << tmp;
		}
		fs << "}";
	}

	template <typename T>
	static void readVectorOfVector(FileStorage &fns, string name, vector<vector<T>> &vectorOfVector)
	{
		vectorOfVector.clear();
		FileNode fn = fns[name];
		if (fn.empty()){
			return;
		}

		FileNodeIterator current = fn.begin(), it_end = fn.end(); // Go through the node
		for (; current != it_end; ++current)
		{
			vector<T> tmp;
			FileNode item = *current;
			item >> tmp;
			vectorOfVector.push_back(tmp);
		}
	}


	virtual ~ChessboardUtils()
	{
	}

private:
	static double PCFreq;
	static __int64 CounterStart;

};

double ChessboardUtils::PCFreq = 0.0;
_int64 ChessboardUtils::CounterStart = 0;

