import glob, os, re

def q(s):
    return '"%s"' % s

def list3(dir):
    kPattern = r'^infra\d{3}\.jpg$'
    cPattern = r'camera%s.jpg'
    dPattern = r'depth%s.csv'
    res = [f for f in os.listdir(dir) if re.search(kPattern, f)]
    i = 0
    print "{"
    for pathAndFilename in res:
        kName, ext = os.path.splitext(os.path.basename(pathAndFilename))
        # find seq
        allSeq = re.findall(r'\d{3}', kName)
        if (allSeq):
            seq = allSeq[0]
            print "\t{", ','.join([q('/'.join([dir, pathAndFilename]))  , q('/'.join([dir, cPattern % seq])), q('/'.join([dir, dPattern % seq]))]), "}",
        else:
            continue
        i = i + 1
        if i < len(res):
            print ","
        else:
            print
        #os.rename(pathAndFilename, 
        #          os.path.join(dir, titlePattern % title + ext))
    print "}"

def list4(dir):
    kPattern = r'^infra\d{3}\.jpg$'
    cPattern = r'camera%s.jpg'
    dPattern = r'depth%s.csv'
    bPattern = r'bindex%s.csv'
    res = [f for f in os.listdir(dir) if re.search(kPattern, f)]
    i = 0
    print "{"
    for pathAndFilename in res:
        kName, ext = os.path.splitext(os.path.basename(pathAndFilename))
        # find seq
        allSeq = re.findall(r'\d{3}', kName)
        if (allSeq):
            seq = allSeq[0]
            print "\t{", ','.join([q('/'.join([dir, pathAndFilename]))  , q('/'.join([dir, cPattern % seq])), q('/'.join([dir, dPattern % seq])),q('/'.join([dir, bPattern % seq]))]), "}",
        else:
            continue
        i = i + 1
        if i < len(res):
            print ","
        else:
            print
        #os.rename(pathAndFilename, 
        #          os.path.join(dir, titlePattern % title + ext))
    print "}"

list3(r"Resources/Dataset 5.1")