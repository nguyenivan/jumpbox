// CalibrationTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ChessboardCalibrator.h"
#include "ChessboardUtils.h"
#include <boost\filesystem.hpp>
#include <typeinfo>


using namespace cv;
using namespace std;
namespace fs = boost::filesystem;
//using std::cin;

const char* pathToChar(fs::path path) {
	string tempString = path.string();
	const char* ret = tempString.c_str();
	return ret;
}

vector<CalibrationSet> sample{
	{ "Resources/Dataset 5.1/infra002.jpg.csv", "Resources/Dataset 5.1/camera002.jpg", "Resources/Dataset 5.1/depth002.csv" },
	{ "Resources/Dataset 5.1/infra003.jpg.csv", "Resources/Dataset 5.1/camera003.jpg", "Resources/Dataset 5.1/depth003.csv" },
	{ "Resources/Dataset 5.1/infra004.jpg.csv", "Resources/Dataset 5.1/camera004.jpg", "Resources/Dataset 5.1/depth004.csv" },
	{ "Resources/Dataset 5.1/infra005.jpg.csv", "Resources/Dataset 5.1/camera005.jpg", "Resources/Dataset 5.1/depth005.csv" },
	{ "Resources/Dataset 5.1/infra006.jpg.csv", "Resources/Dataset 5.1/camera006.jpg", "Resources/Dataset 5.1/depth006.csv" },
	{ "Resources/Dataset 5.1/infra007.jpg.csv", "Resources/Dataset 5.1/camera007.jpg", "Resources/Dataset 5.1/depth007.csv" },
	{ "Resources/Dataset 5.1/infra008.jpg.csv", "Resources/Dataset 5.1/camera008.jpg", "Resources/Dataset 5.1/depth008.csv" },
	{ "Resources/Dataset 5.1/infra009.jpg.csv", "Resources/Dataset 5.1/camera009.jpg", "Resources/Dataset 5.1/depth009.csv" },
	{ "Resources/Dataset 5.1/infra010.jpg.csv", "Resources/Dataset 5.1/camera010.jpg", "Resources/Dataset 5.1/depth010.csv" },
	{ "Resources/Dataset 5.1/infra011.jpg.csv", "Resources/Dataset 5.1/camera011.jpg", "Resources/Dataset 5.1/depth011.csv" },
	{ "Resources/Dataset 5.1/infra012.jpg.csv", "Resources/Dataset 5.1/camera012.jpg", "Resources/Dataset 5.1/depth012.csv" },
	{ "Resources/Dataset 5.1/infra013.jpg.csv", "Resources/Dataset 5.1/camera013.jpg", "Resources/Dataset 5.1/depth013.csv" },
	{ "Resources/Dataset 5.1/infra014.jpg.csv", "Resources/Dataset 5.1/camera014.jpg", "Resources/Dataset 5.1/depth014.csv" },
	{ "Resources/Dataset 5.1/infra015.jpg.csv", "Resources/Dataset 5.1/camera015.jpg", "Resources/Dataset 5.1/depth015.csv" },
	{ "Resources/Dataset 5.1/infra016.jpg.csv", "Resources/Dataset 5.1/camera016.jpg", "Resources/Dataset 5.1/depth016.csv" },
	{ "Resources/Dataset 5.1/infra017.jpg.csv", "Resources/Dataset 5.1/camera017.jpg", "Resources/Dataset 5.1/depth017.csv" },
	{ "Resources/Dataset 5.1/infra018.jpg.csv", "Resources/Dataset 5.1/camera018.jpg", "Resources/Dataset 5.1/depth018.csv" },
	{ "Resources/Dataset 5.1/infra019.jpg.csv", "Resources/Dataset 5.1/camera019.jpg", "Resources/Dataset 5.1/depth019.csv" },
	{ "Resources/Dataset 5.1/infra020.jpg.csv", "Resources/Dataset 5.1/camera020.jpg", "Resources/Dataset 5.1/depth020.csv" },
	{ "Resources/Dataset 5.1/infra021.jpg.csv", "Resources/Dataset 5.1/camera021.jpg", "Resources/Dataset 5.1/depth021.csv" },
	{ "Resources/Dataset 5.1/infra022.jpg.csv", "Resources/Dataset 5.1/camera022.jpg", "Resources/Dataset 5.1/depth022.csv" },
	{ "Resources/Dataset 5.1/infra023.jpg.csv", "Resources/Dataset 5.1/camera023.jpg", "Resources/Dataset 5.1/depth023.csv" },
	{ "Resources/Dataset 5.1/infra024.jpg.csv", "Resources/Dataset 5.1/camera024.jpg", "Resources/Dataset 5.1/depth024.csv" },
	{ "Resources/Dataset 5.1/infra025.jpg.csv", "Resources/Dataset 5.1/camera025.jpg", "Resources/Dataset 5.1/depth025.csv" },
	{ "Resources/Dataset 5.1/infra026.jpg.csv", "Resources/Dataset 5.1/camera026.jpg", "Resources/Dataset 5.1/depth026.csv" },
	{ "Resources/Dataset 5.1/infra027.jpg.csv", "Resources/Dataset 5.1/camera027.jpg", "Resources/Dataset 5.1/depth027.csv" },
	{ "Resources/Dataset 5.1/infra028.jpg.csv", "Resources/Dataset 5.1/camera028.jpg", "Resources/Dataset 5.1/depth028.csv" },
	{ "Resources/Dataset 5.1/infra029.jpg.csv", "Resources/Dataset 5.1/camera029.jpg", "Resources/Dataset 5.1/depth029.csv" },
	{ "Resources/Dataset 5.1/infra030.jpg.csv", "Resources/Dataset 5.1/camera030.jpg", "Resources/Dataset 5.1/depth030.csv" },
	{ "Resources/Dataset 5.1/infra031.jpg.csv", "Resources/Dataset 5.1/camera031.jpg", "Resources/Dataset 5.1/depth031.csv" }
}
;

vector<CalibrationSet> mini{
	{ "Resources/Dataset 5.1/infra016.jpg.csv", "Resources/Dataset 5.1/camera016.jpg", "Resources/Dataset 5.1/depth016.csv" },
	{ "Resources/Dataset 5.1/infra015.jpg.csv", "Resources/Dataset 5.1/camera015.jpg", "Resources/Dataset 5.1/depth015.csv" },
	{ "Resources/Dataset 5.1/infra008.jpg.csv", "Resources/Dataset 5.1/camera008.jpg", "Resources/Dataset 5.1/depth008.csv" },
	{ "Resources/Dataset 5.1/infra031.jpg.csv", "Resources/Dataset 5.1/camera031.jpg", "Resources/Dataset 5.1/depth031.csv" }
}
;

void Test1(){

	ChessboardCalibrator calib;

	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> kinectPoints;
	vector<vector<Point2f>> cameraPoints;
	vector<vector<ushort>> depthPoints;
	Size kinectSize;
	Size cameraSize;
	vector<Mat> RVecs;
	vector<Mat> TVecs;

	FileStorage load("Resources/corners.yaml", FileStorage::READ);

	ChessboardUtils::readVectorOfVector(load, "kinectPoints", kinectPoints);
	ChessboardUtils::readVectorOfVector(load, "cameraPoints", cameraPoints);
	ChessboardUtils::readVectorOfVector(load, "depthPoints", depthPoints);
	ChessboardUtils::readVectorOfVector(load, "objectPoints", objectPoints);
	load["kinectSize"] >> kinectSize;
	load["cameraSize"] >> cameraSize;
	load.release();

	Mat kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers;

	//calib.calibrate(sample, kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers);


	// Calibrate to find kinect matrix
	calibrateCamera(objectPoints, kinectPoints, kinectSize, kinectMatrix, kinectDist, RVecs, TVecs);

	// Calibrate to find camera matrix
	calibrateCamera(objectPoints, cameraPoints, cameraSize, cameraMatrix, cameraDist, RVecs, TVecs);

	cout << "Kinect Matrix" << endl << kinectMatrix << endl;
	cout << "Kinect Distortion" << endl << kinectDist << endl;
	cout << "Camera Matrix" << endl << cameraMatrix << endl;
	cout << "Camera Distortion" << endl << cameraDist << endl;

	cout << "Done!\n";

	cin.ignore();
}

void Test2() {
	ChessboardCalibrator calib;

	const char* calibrationConfig = "Resources/Calibration/calibration.yaml";
	const char* backgroundImage = "Resources/Calibration/background.jpg";
	const char* cameraImage = "Resources/Calibration/camera002.jpg";
	const char* depthData = "Resources/Calibration/depth002.csv";
	const char* outputImage = "Resources/Calibration/jump002.jpg";
	const char* bodyIndex = "Resources/Calibration/bindex002.csv";
	int nearClipPlane = 2000;
	int farClipPlane = 3000;
	bool success = calib.filterCombine(
		calibrationConfig,
		backgroundImage,
		cameraImage,
		depthData,
		outputImage,
		bodyIndex,
		nearClipPlane,
		farClipPlane
		);
}

void Test3() {
	 //Write test data to corners.yaml file
	namedWindow("Test");

	ChessboardCalibrator calib;

	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> kinectPoints;
	vector<vector<Point2f>> cameraPoints;
	vector<vector<ushort>> depthPoints;
	Size kinectSize;
	Size cameraSize;

	vector<Point3f> baseObjectPoints = calib.initObjectPoints(calib.BOARD_SIZE);

	calib.getCornersAndObjectPoints(sample, baseObjectPoints,
		kinectPoints, cameraPoints, depthPoints, objectPoints, kinectSize, cameraSize);

	FileStorage save("Resources/corners.yaml", FileStorage::WRITE);

	ChessboardUtils::writeVectorOfVector(save, "kinectPoints", kinectPoints);
	ChessboardUtils::writeVectorOfVector(save, "cameraPoints", cameraPoints);
	ChessboardUtils::writeVectorOfVector(save, "depthPoints", depthPoints);
	ChessboardUtils::writeVectorOfVector(save, "objectPoints", objectPoints);
	save << "kinectSize" << kinectSize;
	save << "cameraSize" << cameraSize;

	destroyAllWindows();
	
	save.release();

	cout << "Done!\n";

	cin.ignore();

	//Mat kinectMatrix;
	//Mat kinectDist;
	//Mat cameraMatrix;
	//Mat cameraDist;
	//vector<Mat> RVecs;
	//vector<Mat> TVecs;

	//// Calibrate to find kinect matrix
	//calibrateCamera(objectPoints, kinectPoints, kinectSize, kinectMatrix, kinectDist, RVecs, TVecs);

	//// Calibrate to find camera matrix
	//calibrateCamera(objectPoints, cameraPoints, cameraSize, cameraMatrix, cameraDist, RVecs, TVecs);

	//// Get new object points in world coordinate
	//vector<vector<Point3f>> worldPoints;
	//calib.getWorldPoints(kinectPoints, depthPoints, kinectMatrix, kinectDist, worldPoints);
	//

	////Now calculate RVecs and TVecs or camera against kinect depth world using PnP Ransac
	////calibrateCamera(worldPoints, cameraPoints, cameraSize, newMatrix, newDist, RVecs, TVecs);

	//vector<Point3f> newWorldPoints;
	//for (auto v : worldPoints) {
	//	newWorldPoints.insert(end(newWorldPoints), begin(v), end(v));
	//}

	//vector<Point2f> newCameraPoints;
	//for (auto v : cameraPoints) {
	//	newCameraPoints.insert(end(newCameraPoints), begin(v), end(v));
	//}

	//Mat cameraInliers;
	//Mat RVec;
	//Mat TVec;
	//solvePnPRansac(newWorldPoints, newCameraPoints, cameraMatrix, cameraDist, RVec, TVec, false, 100, 8.0f, 100, cameraInliers);

	//// Write test data to corners.yaml file

	//FileStorage save("Resources/corners.yaml", FileStorage::WRITE);

	//ChessboardUtils::writeVectorOfVector(save, "kinectPoints", kinectPoints);
	//ChessboardUtils::writeVectorOfVector(save, "cameraPoints", cameraPoints);
	//ChessboardUtils::writeVectorOfVector(save, "depthPoints", depthPoints);
	//ChessboardUtils::writeVectorOfVector(save, "objectPoints", objectPoints);
	//save << "kinectSize" << kinectSize;
	//save << "cameraSize" << cameraSize;
	//save << "cameraInliers" << cameraInliers;
	//save << "RVec" << RVec;
	//save << "TVec" << TVec;
	//save.release();
	//


	//cout << kinectMatrix << endl;
	//cout << kinectDist << endl;
	//cout << cameraMatrix << endl;
	//cout << cameraDist << endl;
	//cout << cameraInliers << endl;
}

void Test4(){
	ChessboardCalibrator calib;

	Mat kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers;

	calib.calibrate(sample, kinectMatrix, kinectDist, cameraMatrix, cameraDist, cameraRVec, cameraTVec, cameraInliers);

	FileStorage save("Resources/calibration.yaml", FileStorage::WRITE);

	save << "kinectMatrix" << kinectMatrix;
	save << "kinectDist" << kinectDist;
	save << "cameraMatrix" << cameraMatrix;
	save << "cameraDist" << cameraDist;
	save << "cameraRVec" << cameraRVec;
	save << "cameraTVec" << cameraTVec;
	save << "cameraInliers" << cameraInliers;
	save.release();
	

	cout << "Kinect Matrix" << endl << kinectMatrix << endl;
	cout << "Kinect Distortion" << endl << kinectDist << endl;
	cout << "Camera Matrix" << endl << cameraMatrix << endl;
	cout << "Camera Distortion" << endl << cameraDist << endl;

	cout << "Done!\n";

	cin.ignore();
}


int _tmain(int argc, _TCHAR* argv[])
{
	Test2();
	return 0;
}

